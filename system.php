<?php 
	$pageTitle = "Stevie is Sorry.";
	$pageurl = "system";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<?php require('home/head.php'); ?>
	</head>
	<body onload="allLoaded();">
		<div id="whiteZone">
		</div>

		<div id="canvas">
			<div id="topBar">
				<?php require('home/topbar.php'); ?>
			</div>
			<div id="main">		
				<div class="headline">
				</div>		
				<div class="simpleText" style='width: 700px'>
                                    Stevie's too busy to handle all the requests right now.<br/>
                                    Either that or it's something we said. <br/><br/>
                                    Please wait a few minutes and try again while we cool down our servers.
                                </div>
			</div>
                        <div id="fixedBottomBarItems">
                                <?php require('home/bottombar.php'); ?>
                        </div>
		</div>
    <?php
        require_once '/home/adroll.php';
    ?>
	</body>
</html>
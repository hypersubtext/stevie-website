<?php 
    
    require_once($_SERVER['DOCUMENT_ROOT'] . "/lib/domains.php");

    $pageType = 'textPage';
    $pageTitle = 'About Stevie';
    $pageurl = "about";
?>

<!DOCTYPE HTML>
<html>
	<head>
        <?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/head.php'); ?>
	</head>
	<body class="room">
		<div id="page" class="<?= $pageurl ?>">
			<?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/top_nav.php'); ?>
			<div id="main" class="container">
				<div class="panel">
					<h1 id="<?= $pageurl ?>" style="background-image : url(lib/style/<?= $pageurl ?>_title.png)"><?= $pageTitle ?></h1>
					<h2 id="about_title">Stevie - Turning the Social Web into Beautiful TV</h2>
					<iframe id="this_is_stevie" width="560" height="315" src="//www.youtube.com/embed/PIM0N8KXKxQ" frameborder="0" allowfullscreen></iframe>
					<p>
						Stevie is a platform that turns your social feeds into broadcast television, creating personal, monetizable entertainment across platforms.<br/>
						The Stevie engine analyzes everything the viewers and their friends share, to determine relevance and taste. Along with feeds from brands, artists and celebrities, Stevie's patent pending line up algorithm creates 24/7 viewing experience. The Me Show, Friends TV and Music Non Stop are strictly personal channels, creating a unique experience for every viewer. <br/>
						The Stevie Guide includes hundreds of channels based on popular Facebook pages, hashtags and Twitter accounts and provide an endless lean back TV experience.
					</p>
					<p>
						Stevie runs on HTML5 and native applications on different operating systems. Web, iOS, Android and Windows 8 versions are available, Samsung Smart TV version will be available soon.
					</p>

					<h2 id="company_overview">Company Overview</h2>
					<p>
						The Stevie team began working in October 2011, securing $600k from angel investors including Jeff Pulver, Gigi Levy, Simi Efrati &amp; Asi Schmeltzer.<br/>
						Stevie graduated in September from the first Microsoft Accelerator for Azure program for innovative start ups in Tel Aviv, Israel, and launched on stage as a finalist at TechCrunch Disrupt NYC 2012.<br/>
						In April 2013, Stevie won the MIPCube Lab Competition at the MIPTV conference in Cannes, France. MIPCube Lab is an international competition featuring 10 startups that are helping to change the face of the TV industry.<br/>
			            In December 2012 Stevie successfully raised $1.5 million Series A round from Horizons Ventures, the Hong Kong based fund that invested in Spotify, Facebook and Siri (pre-acquisition by Apple), etc.
					</p>
					<p>
						The Stevies have a collective experience of 15 years of designing and engineering successful web, internet and TV products.
					</p>
					<a id="team"></a>
					<h4>Yael Givon, Co Founder, CEO</h4>
					<p>
						A seasoned web creative and experienced entrepreneur, Yael was director of marketing at ICQ (AOL), VP product at Speedbit, consulted many startups on product marketing strategies and previously co-founded Sense of Fashion, a social commerce marketplace for independent fashion.
					</p>
					<h4>Gil Rimon, Co Founder, Chief Creative Technologies</h4>
					<p>
						Gil has broadcast TV writing, editing and presenting on his resume, along with a strong technical background and a number of startups around connected TV and social platforms. Yael and Gil are married and have a baby boy.
					</p>

	                <h2 id="the_team">Meet the Team</h2>
					<div class="row">
						<div class="teamMember col-xs-4">
							<img id="yaelThumbnail" src="http://static.mystevie.com/png/website/yaelthumb.png"/>
							<b>Yael Givon | Founder, CEO</b>
						</div>		
						<div class="teamMember col-xs-4">
							<img id="gilThumbnail" src="http://static.mystevie.com/png/website/gilthumb.png"/>
							<b>Gil Rimon | Founder, Creative Technology</b>
						</div>		
						<div class="teamMember col-xs-4">
							<img id="assafThumbnail" src="http://static.mystevie.com/png/website/assafthumb.png"/>
							<b>Assaf Oppenheimer | CTO</b>
						</div>		
						<div class="teamMember col-xs-4">
							<img id="davidThumbnail" src="http://static.mystevie.com/png/website/davidthumb.png"/>
							<b>David Silberstein | VP Product</b>
						</div>		
						<div class="teamMember col-xs-4">
							<img id="benjyThumbnail" src="http://static.mystevie.com/png/website/benjythumb.png"/>
							<b>Benjy Cook | Programmer</b>
						</div>		
						<div class="teamMember col-xs-4">
							<img id="mayakesThumbnail" src="http://static.mystevie.com/png/website/mayakesthumb.png"/>
							<b>Maya Kessler | Creative Director</b>
						</div>		
						<div class="teamMember col-xs-4">
							<img id="giltsThumbnail" src="http://static.mystevie.com/png/website/giltsthumb.png"/>
							<b>Gil Tselenchuk | Programmer</b>
						</div>		
						<div class="teamMember col-xs-4">
							<img id="mayakorThumbnail" src="http://static.mystevie.com/png/website/mayakorthumb.png"/>
							<b>Maya Korov | Content Manager</b>
						</div>		
						<div class="teamMember col-xs-4">
							<img id="daphnaThumbnail" src="http://static.mystevie.com/png/website/daphnathumb.png"/>
							<b>Daphna Naparstek | Programmer</b>
						</div>
					</div> <!-- team -->		
				</div> <!-- panel -->
			</div>	<!-- main -->	
		</div> <!-- page -->
	    <div id="footer">
			<?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/footer.php'); ?>
		</div>
	</body>
</html>

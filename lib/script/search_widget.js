
function requestClientProgram(channel) {
	var pckg = {"cmd":"requestVODChannel","channel" : channel}
	$("#stvplayer")[0].contentWindow.postMessage(pckg,"*");
    console.log("channel change",channel);
}


$(document).ready(function() {
	// Search Widget
	$.getScript(clientPath+'/web_common/search.js', function() {
	    setTimeout(function() {
	    searchWidget = new Search(
	    	clientPath,
	    	$("#search_input"), // input dom (jQuery plz)
	    	$("#search_results"), // results dom
	    	"website", // website or client
	    	function(channel) {
	    		var pckg = {"cmd":"requestVODChannel","channel" : channel};
	    		$("#stvplayer")[0].contentWindow.postMessage(pckg,"*");
	    		$("#search_input").val(channel.style.name).blur().removeClass('highlight');
	          	$("#search_results").html("");
	          	$('iframe#stvplayer').focus();
	    	} // callback function for ordering a channel
	    	);
		},200)
	});

	$("#search_input").on("focus",function(){
	    $(this).one('mouseup', function(event){
	        event.preventDefault();
	    }).select();
	    $("#search_results").show();
	});

	$("#search_input").on("blur", function(){
		if(!$("#search_results").is(":hover")) {
			$("#search_results").hide();			
		}
	});

});


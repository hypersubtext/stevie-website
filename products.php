<?php 
    
    require_once($_SERVER['DOCUMENT_ROOT'] . "/lib/domains.php");

    $pageType = 'textPage';
	$pageTitle = "Stevie Products";
    $pageurl = "products";
?>

<!DOCTYPE HTML>
<html>
	<head>
        <?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/head.php'); ?>
	</head>
	<body class="room">
		<div id="page" class="<?= $pageurl ?>">
			<?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/top_nav.php'); ?>
			<div id="main" class="container">
				<div class="panel">
					<h1 id="<?= $pageurl ?>" style="background-image : url(lib/style/<?= $pageurl ?>_title.png)"><?= $pageTitle ?></h1>
					<div class="products_section">
						<p>
						To us, Stevie is about the experience, not so much the hardware or platform. We want people to be able to "watch"    their social feed, as if it was a ‘TV show’, on whatever platform that makes it possible. There are several ways for you to get and use Stevie and you should choose the one that is easiest and most accessible for you. If you’re feet are up - you’re doing it right!<br/>
                                                You can access hundreds of Stevie channels, regardless of the platform you choose. Watch personalized content from your Facebook / Twitter friends, enjoy popular web channels and even create your own channels from any search topic or hashtag
						</p>
						<div class="products_image">
							<img align="right" src="http://static.mystevie.com/png/website/nologbanner.png" />
						</div>
					</div>
					<div class="products_section">
						<h4>Stevie on your browser</h4>
						<p>
                                                On your desktop or laptop PC or mac - simply go to www.stevie.com on your browser. Open a browser tab of your Friends TV or another favorite channel and stay up to date during the day. On a mac, you can Airplay your browser tab to your Apple TV and watch it on your big screen TV while slouching on the couch or in bed.
                                                <a href="http://www.mystevie.com" target="_blank">Try it now!</a></p>
					</div>
					<div class="products_section">
                                            <h4>Stevie for your tablet (iOS / Android / Kindle Fire)</h4>
                                            <p>
                                            Your tablet is a magical device - it’s small enough to be mobile, but can also act as your mini-HDTV. Watch Stevie on the go wherever and whenever you want, or use your tablet as a smart-remote and cast / airplay Stevie to your big screen TV when leisure-ing at home. 
                                            <a style="float:left;" href='https://play.google.com/store/apps/details?id=com.stevie'>
                                            <img width="149" height="40" src='http://cdn.mystevie.com/png/website/channelPages/GooglePlayButton.png' /></a>
                                            <a style="float:left; padding-left:20px;" href='http://itunes.apple.com/us/app/stevie/id547231007'>
                                            <img width="149" height="40" src='http://cdn.mystevie.com/png/website/channelPages/AppStoreButton.png' /></a>
                                            </p>
					</div>
					<div style="clear: both;" class="products_section">
                                            <h4>Stevie for Chromecast and Apple TV (iOS & Android)</h4>
                                            <p>
                                            Got your feet on the couch? Good. Stevie for Chromecast & Apple TV lets you AirPlay/Cast your social feeds to your big screen TV. Use your smartphone as a smart remote to control the action and engage with your friend.                                             
                                            <br/><a style="float:left;" href='https://play.google.com/store/apps/details?id=com.stevie'>
                                            <img width="149" height="40" src='http://cdn.mystevie.com/png/website/channelPages/GooglePlayButton.png' /></a>
                                            <a style="float:left; padding-left:20px;" href='http://itunes.apple.com/us/app/stevie/id547231007'>
                                            <img width="149" height="40" src='http://cdn.mystevie.com/png/website/channelPages/AppStoreButton.png' /></a>
                                            </p>
					</div>
					<div style="padding-top: 10px; clear: both;" class="products_section">
                                            <h4>Stevie for Android TV</h4>
                                            <p>
                                            Get Stevie directly on your Android TV - no need to use your mobile or a set-top-box. Download the Stevie app and make your TV socially aware! Start watching your ‘Friends TV’ channel and create your own customized channels based on any search topic or hashtag. Control it from your TV remote.<br/>
                                            [Coming Soon]
                                            </p>
					</div>
					<div style="clear: both;" class="products_section">
                                            <h4>Stevie for Samsung Smart TV (2013 and up)</h4>
                                            <p>
                                            Get Stevie directly on your Samsung Smart TV - no need to use your mobile. Just a few clicks on your TV remote and you can start creating your own channels or watch your social media friends - right there on your Smart TV.
                                            [Coming Soon]
                                            </p>
					</div>
				</div> <!-- panel -->
			</div> <!-- main container -->
		</div> <!-- page -->
	    <div id="footer">
			<?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/footer.php'); ?>
		</div>
	</body>
</html>



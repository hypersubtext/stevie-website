<ul>
<?php
    $selected_menu_item = "home";
    $menu_items = [
        ["home","index.php"],
         ["products","products.php"]
    ];
    foreach($menu_items as $menu_item)
    {
        echo "<li id='" . $menu_item[0] . "'";
        if ($menu_item[0] == $selected_menu_item)
            echo " class='active_menu_item'";
        echo "><a href='" . $menu_item[1] . "'>" . $menu_item[0] . "</a>";
        echo "</li>";
    }
?>
</ul>
<?php 
	$pageTitle = "Stevie Settings";
	$pageurl = "settings";
?>
<!DOCTYPE html>
<html>
	<head>
	<?php require('home/head.php'); ?>
	<?php
		if (isset($_GET["platform"])) {
			$currentPlatform = $_GET["platform"];
		} else {
			$currentPlatform = "website";
		}
	?>

	<script type="text/javascript">

		var platform = "<? echo $currentPlatform; ?>";

		$(document).ready(function() {
			if(platform!='website') {
				$("body").addClass('mobile');
				// trigger click on enter
				// if(platform == "android") {
				// 	$("body").on("keydown", function(e) {
				// 		if(e.keyCode == 13) {
				// 			alert($("*:focus").attr("class"));
				// 		}
				// 	});
				// }
			}


        	$("html").bind("ajaxStart", function() {
          		$(this).addClass('busy');
        	}).bind("ajaxStop", function() {
          		$(this).removeClass('busy');
        	});
        });

		// mailing lists
		function getListName(listID, defaultName) {
			switch (listID) {
				case '58a3af5030':
				return 'New features, new channels, and stuff related directly to you';
				break;
				case 'aac5fa51fa':
				return 'The Stevie newsletter with tips and special offers';
				break;
				default:
				return defaultName;
			}
		}

		function toggleMailing(checkObject, subscribeURL, unsubscribeURL) {
			var ajaxurl;
			var failMessage = '';
			var checked = checkObject.checked;
			var diditwork = false;
			if (checked != true) {
				ajaxurl = unsubscribeURL;
				failMessage = 'unsubscribe failed';
			}
			else {
				ajaxurl = subscribeURL;
				failMessage = 'subscribe failed';
			}
			jQuery.ajax({
				url: ajaxurl,
				success: function(resp) {
					console.log(resp);
					diditwork = (jQuery.parseJSON(resp).status=='200');

					if (!diditwork) {
						alert(failMessage);
					}
					else {
						setObj = (jQuery.parseJSON(resp).description);
						console.log(setObj.mailinglists);
						drawSettings(setObj);
					}
				},
				async: true,
				datatype: 'json',
				xhrFields: {
					withCredentials: true
				}
			});
		}

		// identities
		function removeIdentity(removeURL) {
			var setObj;
			var ajaxurl = removeURL;
			var diditwork = false;
			jQuery.ajax({
				url: ajaxurl,
				success: function(resp) {
					diditwork = (jQuery.parseJSON(resp).status=='200');
					if (!diditwork) {
						alert('There was a problem removing your account, please try again later');
					}
					else {
						setObj = (jQuery.parseJSON(resp).description);
						//console.log(setObj.identities);
						alert('Setting changed successfully. Restart Stevie or wait a few minutes for the changes to take effect');
						drawSettings(setObj);
					}
				},
				async: true,
				datatype: 'json',
				xhrFields: {
					withCredentials: true
				}
			});
		}

      	function toggleConnection(checkObject, disconnectURL, connectURL) {
        	var setObj;
        	var ajaxurl;
        	var checked = checkObject.checked;
        	var diditwork = false;
        	if (checked != true) {
          		ajaxurl = disconnectURL;
        	}
        	else {
          		ajaxurl = connectURL;
        	}
			jQuery.ajax({
				url: ajaxurl,
				success: function(resp) {
					//console.log(resp);
					diditwork = (jQuery.parseJSON(resp).status=='200');
					console.log(diditwork);
					if (!diditwork) {
						alert ('There was a problem with this service, please try again later');
					}
					else {
						setObj = (jQuery.parseJSON(resp).description);
						//console.log(setObj);
						alert ('Setting changed successfully. Restart Stevie or wait a few minutes for the changes to take effect');
                		drawSettings(setObj);
					}
				},
				async:true,
				datatype: 'json',
				xhrFields: {
					withCredentials: true
				}
			});
      	}

		function disableCheckbox() {
			// Disable accountOnOff if only one identity is active
			if ($("input.accountOnOff:checkbox:checked").length <= 1) {
				$("input.accountOnOff:checkbox:checked").prop('disabled', true);
			}
		}

		function drawSettings(data) {
	        var row = '';
	        var primary = '';
	        var others = '';
	        var count = data.identities.length;
	        var idn;
			var mailistData = data.mailinglists;
			var isMailist = (mailistData!=undefined);
			var oneList;

	        // first clear the previous settings
	        $("#account_settings, #mail_settings").hide();
        	$("#account_settings ul, #mail_settings").html('');
	        
	        // then draw a row for each account
        	for ( var i = 0; i < count; i++ ) {
          		idn = data.identities[i];
          		row = '<li class="row ' + idn.identity_name + '"><img class="idn_brdr" src="http://static.mystevie.com/png/website/shadow2.png" />';
          		if (idn.show_content == undefined) {
            	// if user is not logged in to account, display account login button
            		row += '<a class="login_button" href="' + idn.identity_add_url + '">Connect your ' + idn.identity_name + ' account</a>';
          		}
          		else {
          		// when user is logged in, show identity details
            		var appIcon = '<div class="app_icon"><img src="' + idn.identity_image + '"/></div>';
            		var userPic = '<div class="user_pic"><img src="' + idn.profile_image + '"/></div>';
            		var accountDetails = '<div class="account_details">';
            		var userName = '<div class="user_name">' + idn.name + '</div>';
            		var disconnectLink = '';
            		var toggleAccount = '';
            		// place link to remove only on non-prime identities
            		if (idn.is_prime != true) {
		            	disconnectLink = '<a class="remove_link" href="javascript:removeIdentity(\'' + idn.identity_remove_url + '\')">Remove</a>';
            		}
		            toggleAccount = '<div class="toggle_account"><input class="accountOnOff" onkeyup="if(event.which==13) toggleConnection(this,\'' + idn.identity_connect_url + '\',\'' + idn.identity_disconnect_url + '\');" onclick="toggleConnection(this,\'' + idn.identity_disconnect_url + '\',\'' + idn.identity_connect_url + '\')" type="checkbox" ';
            		if (idn.show_content == true) {
              			toggleAccount += 'checked';
            		}
            		toggleAccount += ' /> Show content from ' + idn.identity_name + '</div>'
            		accountDetails = accountDetails + userName + disconnectLink + toggleAccount + '</div>';
            		row += appIcon + userPic + accountDetails;
          		}
          		row += '</li>';
          		if (idn.is_prime != true) {
            		// list all non primary accounts
            		others = others + row;
          		}
          		else {
            		// set the primary account
            		primary = row;
          		}
        	}

        	// draw mailing list settings
			if (isMailist) {
				var mailingChecked = '<h3>Stevie will only send you email about:</h3>';
				for (var listn=0;listn<mailistData.length;listn++) {
					oneList = mailistData[listn];
					mailingChecked += '<div class="toggleMailingDiv">';
					if (oneList.subscribed) {
						mailingChecked += '<input onkeyup="if(event.which==13) toggleMailing(this,\'' + oneList.urls.unsubscribe + '\',\'' + oneList.urls.subscribe + '\');" class="toggleMailing" onclick="toggleMailing(this,\'' + oneList.urls.subscribe + '\',\'' + oneList.urls.unsubscribe + '\')" type="checkbox" checked />';

					}
					else {
						mailingChecked += '<input onkeyup="if(event.which==13) toggleMailing(this,\'' + oneList.urls.unsubscribe + '\',\'' + oneList.urls.subscribe + '\');" class="toggleMailing" onclick="toggleMailing(this,\'' + oneList.urls.subscribe + '\',\'' + oneList.urls.unsubscribe + '\')" type="checkbox" />';
					}
					mailingChecked += ' ' + getListName(oneList.id, oneList.name) +'</div>';

				}
			}

        	$("#account_settings ul").append(primary + others);
        	$("#mail_settings").append(mailingChecked);
        	disableCheckbox();
        	$("#account_settings").fadeIn();
        	$("#mail_settings").fadeIn();
    	}

		function outputSettings(JSONdata) {
			if (!loggedIn) {
				document.location.href = clientPath + 'server/webclient/login.php?stevieurl=' + myHost + '/settings';
			}
			else {
				drawSettings(JSONdata);
			}
		}
	</script>
	<style type="text/css">
		#main:focus { outline: none; border: none; box-shadow: none; }
		a:focus {background: lightblue;}
		a.login_button:focus { border: 1px solid lightblue; }
		body.mobile { width: auto; height: auto; }
		body.mobile #whiteZone { height: 0; overflow: hidden; }
		body.mobile #canvas { width: 900px; left: 20px !important;}
		body.mobile #topBar { display: none; }
		body.mobile #main { top: 50px; padding: 0; font-size: 0.9em;}
		body.mobile #account_settings { width: 450px; }
		body.mobile #mail_settings { width: 430px; margin-left: 20px; }
		body.mobile #mail_settings h3 {  font-size: 20px; }
		body.mobile #mail_settings .toggleMailingDiv { width: auto; }
		body.mobile #bottomBarItems, body.android #fixedBottomBarItems { display: none; }
	</style>
	</head>
	<body onload="allLoaded();">
		<div id="whiteZone"></div>
		<div id="canvas" class="settings">
			<?php 
				if($currentPlatform == 'website') {
					echo '<div id="topBar">';
					require('home/topbar.php');
					echo '</div><div id="fixedBottomBarItems">';
					require('home/bottombar.php');
					echo '</div>';
				}
			?>
			<div id="main" tabindex="0">
				<div id="account_settings">
					<h2>Connected accounts:</h2>
					<ul></ul>
				</div>
				<div id="mail_settings">
				</div>
				<div class="last-focus" tabindex="0" onfocus="this.parentNode.focus()"></div>
			</div>
		</div>
    <?php
        require_once '/home/adroll.php';
    ?>
	</body>
</html>

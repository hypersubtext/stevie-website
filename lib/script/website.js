function getEmbedHeight() {
    var embedHeight = ($("#stevie_embed").width() / 16 * 9);
    embedHeight += 40;
    $("#stevie_embed").css("height" , embedHeight + "px");
}

function expandChannelDesc() {
    var descHeight = ($("#channel_title").height() + $("#desc").height());
    var spanHeihgt = $("#channel_info span").height();
    if (descHeight > spanHeihgt) {
        $("#expand_channel_info").show();
        $("#expand_channel_info").click(function() {
            $("#channel_info span").animate({ height: (descHeight+12) }, 400 , function() {
                $("#expand_channel_info").hide();
                $("#contract_channel_info").show();
            });
        });

        $("#contract_channel_info").click(function() {
            $("#channel_info span").animate({ height: spanHeihgt }, 400, function() {
                $("#contract_channel_info").hide();
                $("#expand_channel_info").show();               
            })
        });
    }
}

$(window).bind("load", function() {
    expandChannelDesc();
});


$(document).ready(function() {

    // add active state to current menu link
    $('#menu a[href*="'+pageurl+'"]').addClass('active');

    getEmbedHeight();
    // expandChannelDesc();

    $( window ).resize(function() {
        getEmbedHeight();
    });

    $('#awards_slider').carousel({
      interval: 8000
    });

    $('#android_help').carousel({
        interval: false,
        wrap: false
    });

});



var messageEventHandler = function(e){
     var packet = e.data;  
     console.log("====> HI! client recieved packet",e.data)
     //$("#search_results").html("");
     if(packet.nowPlaying) {
        whatPlaying = packet.nowPlaying;
        //check if channel playing is in list
        $("#channel_title").text(packet.nowPlaying.style.name);
        $("#sidebar #desc").text(packet.nowPlaying.description);
     }
}
window.addEventListener('message', messageEventHandler,false);

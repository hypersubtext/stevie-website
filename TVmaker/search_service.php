<?php
//    error_reporting(E_ALL);
//    ini_set('display_errors', '1'); 

// Load all libs
    require_once 'facebook.php';
    require_once 'TwitterAPIExchange.php';
    require_once 'Google/Client.php';
    require_once 'Google/Service/YouTube.php';

 // Get query parameters
    $query = $_GET["q"];
    if (isset($_GET['limit'])) {
        $limit = $_GET['limit'];
    }
    else {
        $limit = 5;
    }
    $tw_limit = $limit;
    $fb_limit = $limit;
    $yt_limit = $limit;
    if (isset($_GET['batch'])) {
        $batch = $_GET['batch'];
    }
    else {
        $batch = 1;
    }
    

    $sources = "";
    $sourceID = $batch*100;
// set keys
    $YT_DEVELOPER_KEY = 'AIzaSyAAE9ykVachv1FGTF9CoTGaj1v8WOay80A';
    $twitter_settings = array(
        'oauth_access_token' => "2558821686-LZ8alCkAeugXRhTUryVa24Hx4VS7O5gCVFrmYuw",
        'oauth_access_token_secret' => "4uZQpFkmqWKFNxPhLmtmzOYxnQFyG9YO10SyfH1kxhgQQ",
        'consumer_key' => "RhTAowwEDPW3EZZHWExTAY3aU",
        'consumer_secret' => "nyZz181gIkOsgyiuP87E1vyoUr7DxIZqkdAT93mgK8qH35Ac6r"
    );
    $facebook_config = array(
        'appId' => '742461772485499',
        'secret' => '15547e16b9c1f9b75d624c0a32280280',
      );

// set sn icons
    $twitter_icon_img = "http://b22591451a4d0d38ade0-f179ab4fb1bfcc8177dcceba5c435bea.r18.cf2.rackcdn.com/png/twitter.png";
    $facebook_icon_img = "http://b22591451a4d0d38ade0-f179ab4fb1bfcc8177dcceba5c435bea.r18.cf2.rackcdn.com/png/Facebook.png";
    $youtube_icon_img = "https://9105ed55328365fe9fab-f179ab4fb1bfcc8177dcceba5c435bea.ssl.cf2.rackcdn.com/webclient/social/YouTube.png";
    $search_icon_img = "http://b22591451a4d0d38ade0-f179ab4fb1bfcc8177dcceba5c435bea.r18.cf2.rackcdn.com/png/search.png";
    
// init sn objects    
    $yt_client = new Google_Client();
    $yt_client->setDeveloperKey($YT_DEVELOPER_KEY);
    $youtube = new Google_Service_YouTube($yt_client);

    $facebook = new Facebook($facebook_config);
    $fb_access_token = $facebook->getAccessToken();

    $twitter = new TwitterAPIExchange($twitter_settings);

    // get sn results
    
    try {
        $yt_searchResponse = $youtube->search->listSearch('id,snippet', array(
      'q' => $query,
      'type' => 'channel',
      'maxResults' => $yt_limit,
    ));
    }
    catch (Exception $e) {
        $youtube = null;
    }
//    var_dump($yt_searchResponse);
    try {
        $fb_ret_obj_full = $facebook->api(
            '/search',
            'GET',
            array(
                'access_token' => $fb_access_token,
                'q' => $query,
                'type' =>'page',
                'limit' => $fb_limit
            )
        );
        $fb_ret_obj = $fb_ret_obj_full["data"];
    }
   catch (Exception $e) {
        $fb_ret_obj = null;
   }
    
    try {
        $twitter_requestURL = 'https://api.twitter.com/1.1/users/search.json';
        $twitter_requestMethod = 'GET';
        $twitter_getfield = '?q=' . urlencode($query) . '&count=' . $tw_limit;
        $twitter_results =  $twitter->setGetfield($twitter_getfield)
                     ->buildOauth($twitter_requestURL, $twitter_requestMethod)
                     ->performRequest(); 
        $twitter_array = json_decode($twitter_results,true);
    }
    catch (Exception $e) {
        $twitter_array = null;
    }
    
    
    
    function addSource($sourceTitle, $sourceURL, $thumb, $serviceimgurl) {
        global $sources, $sourceID;
        $out = "";
        $out = "<img width='40' height='40' class='thumbnail' align='left' valign='middle' style='position:relative;top:-10px;' src='" . $thumb . "'/>";
        $out .= "<img width='40' height='40' class='service_icon' align='left' valign='middle' style='position:relative;top:-10px;' src='" . $serviceimgurl . "'/>";
        $out .= $sourceTitle;
        ++$sourceID;
        $sources .= '{"name":"' . $out . '","url":"' . $sourceURL . '","id":"' . $sourceID . '"},';
    }

    
    echo '{"status":200,"description":';
    echo '{"suggestedSources":[';

    
            $sourceTitle = "Add all tweets with '" . $query . "'";
            $sourceURL = 'https://twitter.com/search?q=' . $query;
            $sourceImage = $search_icon_img;
            $snImage = $twitter_icon_img;
            addSource($sourceTitle, $sourceURL, $sourceImage, $snImage);
            
            if (!is_null($twitter_array)) {
                for ($i = 0; $i <= count($twitter_array)-1; $i++) {
                    $sourceTitle = $twitter_array[$i]['name'];
                    $sourceURL = 'https://twitter.com/' . $twitter_array[$i]['screen_name'];
                    $sourceImage = $twitter_array[$i]['profile_image_url'];
                    $snImage = $twitter_icon_img;
                    addSource($sourceTitle, $sourceURL, $sourceImage, $snImage);
                }
            }

            if (!is_null($fb_ret_obj)) {
                for ($i = 0; $i <= count($fb_ret_obj)-1; $i++) {
                    $sourceTitle = $fb_ret_obj[$i]['name'];
                    $sourceURL = "https://facebook.com/" . $fb_ret_obj[$i]['id'];
                    $sourceImage = 'https://graph.facebook.com/' . $fb_ret_obj[$i]['id'] . '/picture?width=40&amp;height=40';
                    $snImage = $facebook_icon_img;
                    addSource($sourceTitle, $sourceURL, $sourceImage, $snImage);
                }
            }
            
            $sourceTitle = "Add all clips with '" . $query . "'";
            $sourceURL = 'https://www.youtube.com/results?search_query=' . $query;
            $sourceImage = $search_icon_img;
            $snImage = $youtube_icon_img;
            addSource($sourceTitle, $sourceURL, $sourceImage, $snImage);

            if (!is_null($youtube)) {
                foreach ($yt_searchResponse['items'] as $yt_searchResult) {
                    $sourceTitle = $yt_searchResult['snippet']['title'];
                    $sourceURL = "https://www.youtube.com/channel/" . $yt_searchResult['id']['channelId'];
                    $sourceImage = $yt_searchResult['snippet']['thumbnails']['medium']['url'];
                    $snImage = $youtube_icon_img;
                    addSource($sourceTitle, $sourceURL, $sourceImage, $snImage);
                }
            }
    
    
    //take off last comma
    echo substr($sources,0,strlen($sources)-1);
    
    
?>
]}}
<?php 
    
    require_once($_SERVER['DOCUMENT_ROOT'] . "/lib/domains.php");

    $pageType = 'homePage';
    $pageTitle = 'Stevie - Creating channels from facebook twitter and online video to broadcast on TV. Chromecast coming soon!';
    $pageurl = "home";
?>

<!DOCTYPE HTML>
<html>
	<head>
        <?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/head.php'); ?>
	</head>
	<body class="homeroom">
		<div id="page" class="<?= $pageurl ?>">
			<?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/top_nav.php'); ?>
			<div id="main" class="container">
				<h2 class="main_heading"> Make your TV more awesome!</h2>
				<div class="row">
					<div id="main_image" class="col-md-9">
						<a href="/products"><img src="http://cdn.mystevie.com/website/tvwallblur.png"></a>
						<div id="television"></div>
					</div>
				</div>
			</div>	<!-- main -->	
		</div> <!-- page -->
		<div id="hand">
			<div class="container">
				<a href="/products"><h2 class="sub_heading">Bringing your <br> social feeds and <br> the best of the web <br> to any screen</h2>
				<img src="http://cdn.mystevie.com/website/hand_fb.png"></a>
			</div>
		</div>
	    <div id="footer">
			<?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/footer.php'); ?>
		</div>
	</body>
</html>

<?php 
    //TODO:
    // check that values are valid and that all boxes are filled
    // check if the URL is available, check if there's a Stevie channel with this name
    // designate main FB page. Tell user he has to add a FB page if he didn't
    // add more fields??
    // work on createChannel.php - now I'm gonna send it by mail or something


    require_once($_SERVER['DOCUMENT_ROOT'] . "/lib/domains.php");

    $pageType = 'textPage';
    $pageTitle = 'Create a channel';
    $pageurl = "create";
?>

<!DOCTYPE HTML>
<html>
	<head>
        <?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/head.php'); ?>
        <link rel="stylesheet" href="/followtv/dist/css/bootstrap.min.css" />
        <style>
            .source {
                clear: both;
            }
            #ajaxGIF {
                position: absolute;
                padding-left: 515px;
                padding-top: 5px;
                z-index: 9;
                display: none;
            }
            .sourceItemClass {
                clear: both;
                border: 1px solid #ccc;
                width: 400px;
                padding-top: 15px;
                padding-left: 10px;                    
                background-color: #f5f5f5;
                box-sizing: border-box;
            }
            .sourceItemClass a:link {
                color: #666;
            }
            .sourceItemClass a:visited {
                color: #666;
            }
            .sourceItemClass a:hover {
                color: #666;
            }
            .sourceItemClass a:active {
                color: #666;
            }
            .sourceDelSpan {
                right:5px;
                float: right;
                position: relative;
                font-size: 14px;
                top: -14px;
            }
            .sourceMainSpan {
                font-size: 13px;
                float: right;
            }
            #search_results {
                    color: #666;
                    margin-top: -1px;
                    z-index: 1001;
                    position: relative;
                    max-height: 452px;
                    overflow: auto;
                    z-index: 100;
            }

            #search_results li.source {
                    width: 100%;
                    border: 1px solid #ccc;
                    border-top: none;
                    background-color: #f5f5f5;
                    cursor: pointer;
                    padding: 15px 15px;
                    box-sizing: border-box;
            }

            #search_results li.source:hover, #search_results li.selected_result {
                    background: #f0f0f0;
            }

            #search_results li.source:first-child {
                    border-top: 1px solid #ccc;
            }

            #search_results li.source:last-child {
                    /*border-bottom-right-radius:6px;
                    border-bottom-left-radius:6px;*/
            }

            #search_results li.source span {
                    float: right;
                    color: #666;
            }

            #search_results li.source:focus {
            /*	background-color: yellow;*/
            }


        </style>
        <script>
            window.mainFBsource = 0;
            // Search Widget
            $.getScript('sourceSearch.js', function() {
                setTimeout(function() {
                    searchWidget = new Search(
                    clientPath,
                    $("#newSource"), // input dom (jQuery plz)
                    $("#search_results"), // results dom
                    "website", // website or client
                    function(channel) {
                            var pckg = {"cmd":"requestVODChannel","channel" : channel};
                            $("#stvplayer")[0].contentWindow.postMessage(pckg,"*");
                            $("#newSource").val(channel.style.name).blur().removeClass('highlight');
                    $("#search_results").html("");
                    $('iframe#stvplayer').focus();
                    } // callback function for ordering a channel
                    );
                    },200)
            });

            $("#newSource").on("focus",function(){
                $(this).one('mouseup', function(event){
                    event.preventDefault();
                }).select();
                $("#newSource").show();
            });

            $("#newSource").on("blur", function(){
                    if(!$("#newSource").is(":hover")) {
                            $("#newSource").hide();			
                    }
            })
            function createChannel() {
                if ((window.mainFBSource==0)||(window.mainFBSource==undefined)) {
                    alert ('Please choose your main FB source');
                }
                else {
                    var mainurl = $("#sourceURLitem" + window.mainFBSource).attr('href');
                    var thisURL, newHTML;
                    if (confirm('Are you sure you want to create this channel?')) {
                        $( ".sourceURL" ).each(function( index ) {
                            thisURL = $(this).attr('href');
                            newHTML = '<input type="hidden" name="source' + index + '" value="' + thisURL + '">';
                            $("#sendchannel").append(newHTML);
                        });
                        newHTML = '<input type="hidden" name="mainsource' + '" value="' + mainurl + '">';
                        $("#sendchannel").append(newHTML);
                        $('#sendchannel').submit();
                    };
                }
            }
            
            function makeMainSource(divId) {
                var thisURL;
                var valid = false;
                if (window.mainFBSource!=divId) {
                    $( ".sourceURL" ).each(function( index ) {
                        thisURL = $(this).attr('href');
                        if (thisURL.substr(8,8)=='facebook') {
                            if ($(this).attr('id') == ('sourceURLitem' + divId)) {
                                valid = true;
                            }
                        }
                    });
                    if (valid) {
                        $("#mainLink" + window.mainFBSource).html("Make main source");
                        $("#mainLink" + divId).html("MAIN");
                        window.mainFBSource = divId;
                    }
                }
            }    
            function delSource(divId) {
                if ($("#mainLink" + divId).html == 'MAIN') {
                    window.mainFBsource = 0;
                }
                $("#SIC" + divId).remove();
            }    
            function resetSources() {
                $(".sourceItemClass").remove();
            }    
            </script>
	</head>
	<body class="room">
		<div id="page" class="<?= $pageurl ?>">
			<?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/top_nav.php'); ?>
			<div id="main" class="container">
				<div class="panel">
					<div class="row">
						<div id="contactBox" style="padding-left:35px;" class="col-md-6">
							<form class="form-horizontal" role="form" name="sendchannel" id="sendchannel" method="post" action="createChannel.php">
								<div class="row">
									<h4>Create a new channel</h4>
									<div class="form-group">
										<label for="emailBox" class="col-sm-3 control-label">Channel name</label>
										<div class="col-sm-9">
											<input id="titleBox" class="form-control" placeholder="Title" name="channelname" type="text" />
										</div>
									</div>
									<div class="form-group">
										<label for="subjectBox" class="col-sm-3 control-label">www.stevie.com/</label>
										<div class="col-sm-9">
											<input id="codeBox" class="form-control" placeholder="Requested channel code" name="channelurl" type="text" />
                                                                                </div>
									</div>
									<div class="form-group">
										<label for="emailBox" class="col-sm-3 control-label">Your email</label>
										<div class="col-sm-9">
											<input id="emailBox" class="form-control" placeholder="me@somewhere.com" name="useremail" type="text" />
                                                                                </div>
									</div>
									<div class="form-group">
                                                                                <label for="newSourceBox" class="col-sm-3 control-label">Add source</label>
                                                                                <img id="ajaxGIF" src="http://b22591451a4d0d38ade0-f179ab4fb1bfcc8177dcceba5c435bea.r18.cf2.rackcdn.com/gif/loader.gif" />
										<div class="col-sm-9">
                                  							<input id="newSource" type="text" placeholder="Enter any term to add content sources" class="form-control" name="newsource" />
										</div>
                                						<ul style="padding-left: 155px; width:544px;" id="search_results"></ul>
									</div>
									<span id="resetButton" style="width:158px;" class="btn btn-default btn-lg"><a href="javascript: resetSources();">Reset sources</a></span>
									<span id="sendButton" style="width:158px;" class="btn btn-default btn-lg"><a href="javascript: createChannel();">Create channel</a></span>
								</div>
							</form>
						</div>
                                                <div id="sourceList" class="col-md-6">
                                                    <h4 style="padding-bottom: 10px;">Current source list:</h4>
                                                </div>
					</div>
				</div> <!-- panel -->
			</div>	<!-- main -->	
		</div> <!-- page -->
	    <div id="footer">
			<?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/footer.php'); ?>
		</div>
		<!-- bootstrap js -->
		<script src="/followtv/dist/js/bootstrap.min.js"></script>
	</body>
</html>

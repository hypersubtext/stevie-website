var configObj, settingsObj;
var config = false;
var settings = false;
var myHost = document.domain;
var twinHost = myHost.replace('stevie', 'mystevie');
//if (document.domain == 'localhost') {
//  twinHost = 'dev.mystevie.com';
//}

var isIE9 = ($.browser.msie  && parseInt($.browser.version, 10) <= 9);
var loggedIn = false;
var objurl = 'http://' + twinHost + '/server/services/config.js.php?component=website&platform=web&version=1.0&format=json';

var identitiesInfo = {
  userName : null,
  primeId : null

};

startLoader();

function startLoader() {
  jQuery.ajax({
    url: objurl,
    success: function(resp) {
      configSuccess(resp);
    },
    error: function(resp) {
      if ((resp.readyState==4)&&(resp.status==200)) {
        configSuccess(resp);
      }
      else {
        configFailed();
      }
    },
    async: true,
    datatype: 'json'
  });
}

function configSuccess(oresp) {
  if (oresp.responseText==undefined) {
    resp = oresp;
  }
  else {
    resp = oresp.responseText;
  }
  configObj = jQuery.parseJSON(resp);
  config = true;
  
  var platform = getQueryArgument("platform");
  
  function getQueryArgument(platform) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
      var pair = vars[i].split("=");
      if(pair[0] == platform){return pair[1];}
    }
    return("web");
  }




  var settingsURL = configObj.server_path + 'services/user_settings_service.php?component=website&platform=' + platform + '&version=1&command=get_settings_for_website';

  jQuery.ajax({
    url: settingsURL,
    success: function(resp) {
      settingsSuccess(resp);
    },
    error: function(resp) {
      settingsFailed();
    },
    async:true,
    datatype: 'json',
    xhrFields: {
      withCredentials: true
    }
  });
  if (pageURL=='home') {
    drawVOD();
  }
}

function configFailed() {
  loggedIn = false;
  config = false;
  settings = false;
  handlePages();
}

function settingsSuccess (oresp) {
  resp = jQuery.parseJSON(oresp);    
  settingsObj = resp.description;
  if (settingsObj.code == 'error_user_not_logged_in') {
    loggedIn = false;
  }
  else {
    var userName;
    loggedIn = true;
    var count = settingsObj.identities.length;
    var badIds = 0;
    for (var c = 0; c < count; c++) {
      var idn = settingsObj.identities[c];
      if (typeof(idn.is_prime) != "undefined") {
        if (idn.is_prime == true) {
          userName = idn.name;
          primeId = idn.identity_name;
          break;
        }
      }
      else {
        badIds++;
      }    
    }
    if (badIds == count) {
      loggedIn = false;
    } else {
      identitiesInfo.userName = userName; 
      identitiesInfo.primeId = primeId; 
    }
  }
  settings = true;
  handlePages();
}

function settingsFailed () {
//  alert("settings has failed!");
  loggedIn = false;
  settings = false;
  handlePages();
}

function handlePages() {
  if (pageURL=='settings') {
    outputSettings(settingsObj);
  }
  if (isIndex) {
    drawVOD();
  }
  drawLoggedIn();
}

var ua = navigator.userAgent.toLowerCase();
var isiPad = /ipad/i.test(ua);
var isiPhone = /iphone/i.test(ua);
var isiPod = /ipod/i.test(ua);
var isiOS = (isiPad||isiPhone||isiPod);
var isAndroid = ua.indexOf("android") > -1;
//var isAndroid = /android/i.test(ua);

jQuery.fn.moveToMiddle = function () {
	if(isiPad) return this;
	if ($(window).width()<1024) {
		var delta = $(window).width() - 1024;
		var offset = -200
		if (delta<offset) {
			delta = offset;
		}
		$("#canvas").css('left', delta + 'px');
	    $('body').css("overflow-x","auto");

	}

	else {
	    this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() - 50 + "px");
	    $('body').css("overflow-x","hidden");
	    return this;
	}
};

function showWhiteZone() {
	$("#whiteZone").show();
}

$(window).resize(function() {
	$("#canvas").moveToMiddle();
	});

function wipeLogo () {
    oldLogoID = currentLogoID;
    if (currentLogoID>=6) {
            currentLogoID = 1;		
    }
    else {
            ++currentLogoID;
    }

    nextLogoID = currentLogoID + 1;
    if (nextLogoID>6) {
            nextLogoID = 6;
    }

    var oldElement = $("#logo" + oldLogoID);	
    var domElement = $("#logo" + currentLogoID);	
    var nextElement = $("#logo" + nextLogoID);

    oldElement.css('z-index','0');
    $(".logo").hide();
    oldElement.show();

    var time = 1500;
    domElement.show();
    var max = domElement.width();
    domElement.css("width","0px");
    domElement.css("z-index","100");
    domElement.animate({"width":max+"px"},time, "linear", function () {oldElement.hide();});

    nextElement.html('<img src="http://static.mystevie.com/png/website/logo/logo' + nextLogoID + '.png"/>');
}

function allLoaded() {
        showWhiteZone();
        $(".twitter-follow-button").fadeIn('slow');
        $("#canvas").show();
        $("#logo2").html('<img src="http://static.mystevie.com/png/website/logo/logo2.png"/>');
        if (isIndex) {
            embedSlideShow();
        }
        else
        {
            $("#pageTitle").html(pageTitle);
        }
        drawTopBar();
}

function embedSlideShow() {
        $('#embeddedSlideshow').attr('src', 'home/show.php?run');
}

function embedPromo() {
        $('#embeddedSlideshow').show();
        $('#embeddedSlideshow').attr('src','http://www.youtube.com/embed/39PqsKfBbB0');
}

function startStevie() {
        //document.location.href = clientPath;
        location.href = clientPath;
        setTimeout(function(){startLoader();},8000);
}

function startStevieFriends() {
    _gaq.push(['_link', url_domain(clientPath)]); 
    location.href = clientPath + '/FriendsTV';
    setTimeout(function(){startLoader();},8000);
}
	

$(function() {
    setInterval(function() {
        wipeLogo();
    }, 20000);

    $("#canvas").moveToMiddle();

    $(".greenStevie").mouseover(function() {
    $(".greenStevie").css("color", "#f21c62");
    })
    .mouseout(function() {
    $(".greenStevie").css("color", "#0a0");
    });

    $("#fromYoutubeWatchStevie").mouseover(function() {
    $("#fromYoutubeWatchStevie").css("background-color", "#f21c62");
    })
    .mouseout(function() {
    $("#fromYoutubeWatchStevie").css("background-color", "#080");
    });
});

function logout() {
    var webReallylogout = confirm("Are you sure?\nThis will log you out of Stevie");
    if (webReallylogout) {
            document.location.href = clientPath + 'server/webclient/logout.php?redirect=http://' + document.domain;
    }
}

function showRightButtons (){
        if (loggedIn) {
            $("#loginButtons").hide();
            $("#userLoggedIn").fadeIn('slow');
        }
	else {
            $("#userLoggedIn").hide();
            $("#loginButtons").fadeIn('slow');
	}
	
}

function logoClicked() {
    if (isIndex) {
            wipeLogo();
    }
    else {
            document.location.href='/';
    }
}

function fcClicked() {
	location.href = clientPath + 'server/webclient/facebook_login.php?stevieurl=' + clientPath + '/FriendsTV';
	setTimeout(function(){startLoader();},4000);
}

function tcClicked() {
	location.href = clientPath + 'server/webclient/twitter_login.php?stevieurl=' + clientPath + '/FriendsTV';
	setTimeout(function(){startLoader();},4000);
}

function topMenuItem(title, url) {
    var iHtml = '';
    if (url==pageURL) {
        iHtml += '<span class="selected">' + title + '</span>';
    }
    else {
        if (url=='followtv') {
            iHtml += '<span class="redMenuLink"><a href="' + url + '">' + title + '</a></span>';
        }
        else {
            if (url=='') {
                    iHtml += '<a href="/">' + title + '</a>';
            }
            else {
                    iHtml += '<a href="' + url + '">' + title + '</a>';
            }
        }
    }

    if (url!='contact') {
        iHtml += ' | ';
    }
    return iHtml;
}

function drawTopBar() {
    var html = '';
    html += '<img class="tri" src="http://static.mystevie.com/png/website/tri.png" />';
    html += topMenuItem('Home', '');
    html += topMenuItem('Stevie Channel Guide', 'guide');
    // if (!loggedIn) {
    //         html += topMenuItem('About Us', 'about');
    // }
    html += topMenuItem('FollowTV' , 'followtv');
    html += topMenuItem('Press', 'press');
    html += topMenuItem('Blog', 'http://blog.stevie.com/');
    html += topMenuItem('Help', 'help');
    html += topMenuItem('Send Feedback', 'contact');

    if (loggedIn) {
          // html += '<span id="facebook_buttons">';
          // html += '<span id="logOutLink" style="left: 265px;">';
          var nameHTML ='';
          var primeIcon = '';
          if (pageurl=='settings') {
              nameHTML = '<span class="settingsSelected">' + identitiesInfo.userName + '</span>';
          }
          else {
              nameHTML = '<a id="settingsLinkText" href="settings">' + identitiesInfo.userName + '</a>';
          }
          primeIcon = '<div id="primeAccountIcon" class="' + identitiesInfo.primeId + '"></div>'

          $("#userName").html(nameHTML + primeIcon);
          //html += nameHTML;
    }
    $("#topBarItems").html(html);
    // $("#logOutLink").fadeIn('slow');
}

function drawLoggedIn() {
    drawTopBar();
    showRightButtons();
    if (pageTitle=='home') {
        if (loggedIn) {
            $("#leftImageLI").fadeIn('slow');
            $("#leftImageLO").fadeOut('slow');
            startSlideShow();
        }
        else {
            $("#leftImageLO").fadeIn('slow');
            $("#leftImageLI").fadeOut('slow');
        }
    }
}

function startSlideShow () {
    jQuery('#slideshow').jcarousel({
        auto: 8,
        wrap: 'last',
        scroll: 1,
        buttonNextHTML: null,
        buttonPrevHTML: null,
        itemFallbackDimension: 300,
        itemFirstInCallback: {onBeforeAnimation: setdot}
    });
}

function parametersIn (st) {
    console.log (st);
    console.log (st.indexOf("?"));
    return (st.indexOf("?")>-1);
}

function url_domain(data) {
  var    a      = document.createElement('a');
         a.href = data;
  return a.hostname;
}

function drawVOD() {
    if (loggedIn) {
        var programList = settingsObj.vod_channels;
        var html = "";
        for( var i = 0; i<programList.length; i++) {
            var programURL = programList[i].url;
            var programImgSrc = programList[i].style.website.vodIcon;
/*            if(!(isiPad)) { */
                html+= "<a href='" + programURL + "'><img class='vod_thumb' src='" + programImgSrc + "' /></a>";
/*            }
            else {
                    html+= "<img class='vod_thumb' src='" + programImgSrc + "' />";
            }		 */
        }
    }
    else { 
        var html = '<a href="'+ clientPath +'/FriendsTV"><img class="vod_thumb" src="http://cdn-c.mystevie.com/FriendsTVChannel/platform_specific/website/vod.png"></a>';
        html += '<a href="'+ clientPath +'/Comedy"><img class="vod_thumb" src="http://cdn-c.mystevie.com/ComedyChannel/platform_specific/website/vod.png"></a>';
        html += '<a href="'+ clientPath +'/Music"><img class="vod_thumb" src="http://cdn-c.mystevie.com/MusicChannel/platform_specific/website/vod.png"></a>';
        html += '<a href="'+ clientPath +'/CelebTV"><img class="vod_thumb" src="http://cdn-c.mystevie.com/CelebChannel/platform_specific/website/vod.png"></a>';
        html += '<a href="'+ clientPath +'/MyStevieTV"><img class="vod_thumb" src="http://cdn-c.mystevie.com/StevieFavoritesChannel/platform_specific/website/vod.png"></a>';
        html += '<a href="'+ clientPath +'/u/me"><img class="vod_thumb" src="http://cdn-c.mystevie.com/UserSpotlightChannel/platform_specific/website/vod.png"></a>';
    }
    $("#ondemand_bar").html(html);

    if(isiOS) {
        html = "Stevie turns your Facebook and Twitter into a beautiful television experience!<br>" +
        "<a href='http://itunes.apple.com/us/app/stevie/id547231007?ls=1&mt=8'><img src = 'http://static.mystevie.com/png/website/app_store_main_5.png' style='padding-top: 45px;'/></a><span class='home_below_button' style='font-size: 24px'> </span><br>";
        $('#appsBar').hide();
    }
    else if(isAndroid) {
        html = "Stevie turns your Facebook and Twitter into a beautiful television experience!<br>" +
        "<a href='https://play.google.com/store/apps/details?id=com.stevie'><img src='http://static.mystevie.com/png/website/google_play_btn.png' style='padding-top: 45px;' /></a><span class='home_below_button' style='font-size: 24px'> </span><br>";
        $('#appsBar').hide();
    }
    else if (loggedIn) {
        html = "Stevie turns your Facebook and Twitter into a beautiful television experience!<br/>"+
        "<img onClick='startStevieFriends();' id='startpng' src = 'http://static.mystevie.com/png/website/nstart.png' />";
        // "<div class='home_below_button'>This will take you to sign in with your Facebook account.<br></div>"
        // "<div class='smallprint'>Stevie will never automatically post anything on your behalf.</div>"
    } else {
        html = "Stevie turns your Facebook and Twitter into a beautiful television experience!<br/>"+
        "<img id='startpng' onClick='startStevie();' id='startpng' src = 'http://static.mystevie.com/png/website/nstart.png' />"+
        "<img id='fb_login_button' onClick='fcClicked();' src='http://static.mystevie.com/png/website/facebook_login_home.png'/>"+
        "<img id='tw_login_button' onClick='tcClicked();' src='http://static.mystevie.com/png/website/twitter_login_home.png'/>";
    }


    $("#middleBar").html(html);
    if(isiOS) {
        $("#fconImg").hide();
    }	
}


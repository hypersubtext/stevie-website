<?php

    $channels_used = array();
    header('Content-type: text/xml');
    echo '<?xml version="1.0" encoding="UTF-8"?>';
    echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">';

    outputXMLrecord("http://www.stevie.com/FriendsTV");
    outputXMLrecord("http://www.stevie.com/Comedy");
    outputXMLrecord("http://www.stevie.com/Music");
    outputXMLrecord("http://www.stevie.com/CelebTV");
    outputXMLrecord("http://www.stevie.com/MyStevieTV");
    
    $json_code = file_get_contents('http://www.mystevie.com/server/services/guide_service.php?command=get_guide_channels');
    $cats = json_decode($json_code)->description;

    $catsnum = count($cats);
    
    for($catid=0; $catid<$catsnum; $catid++) {
        $channels = $cats[$catid]->channels;
        $category = $cats[$catid]->category;
        $channum = count($channels);
        
        outputXMLrecordWithImage ($category->url, $category->style->guide_image);

        for($chanid=0; $chanid<$channum; $chanid++) {
            if ($channels[$chanid]->code=="SearchtermSpotlightChannel") {
                outputXMLrecordWithImage ($channels[$chanid]->url,$channels[$chanid]->style->guide_image);
            }
            else {
                outputXMLrecord ($channels[$chanid]->url);
            }
        }
    }
    echo "</urlset>";
 
    
    function outputXMLrecord($st) {
        global $channels_used;
        $fixedst = fixurl($st);
        
        if (!in_array($fixedst, $channels_used)) {
            array_push($channels_used, $fixedst);
            echo "<url><loc>" . $fixedst . "</loc></url>";
        }        
    }
    
    function outputXMLrecordWithImage($st, $img) {
        global $channels_used;
        $fixedst = fixurl($st);
        
        if (!in_array($fixedst, $channels_used)) {
            array_push($channels_used, $fixedst);
            echo "<url><loc>" . $fixedst . "</loc>";
            echo "<image:image><image:loc>" . $img . "</image:loc></image:image></url>";
        }        
    }
    
    function fixurl($url) {
        $url = trim($url);
        $url = str_replace("/on//", "/", $url);
        $url = str_replace("mystevie", "stevie", $url);
        $url = str_replace("%2B", " ", $url);        
        $url =  str_replace("backend.stevie.com", "www.stevie.com", $url);
        return $url;
    }
    
?>

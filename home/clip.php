<?php 
	$pageTitle = "home";
	$pageurl = "";
	$path = '../../server';
	require('userdata.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<?php require('head.php'); ?>
	</head>	
	<body onload="allLoaded(); embedPromo();">

	<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=210504105683030";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>		


<div id="whiteZone">
		</div>

		<div id="canvas">
			<div id="topBar">
				<?php require('topbar.php'); ?>
			</div>
			
			<div id="main">		
				<div id="rightBar">
					<div id="likeFollow">
						<div id="likeFollowHeadline" class="bolder">
							<img class="tri" src="http://static.mystevie.com/png/website/tri.png" />
							Keep in Touch
						</div>
						<div class="fb-like" data-href="https://www.facebook.com/MyStevieTV" data-send="false" data-layout="box_count" data-width="150" data-show-faces="false" data-font="arial"></div>
						<br/><br/>
						<a href="https://twitter.com/MyStevieTV" class="twitter-follow-button" data-show-count="true">Follow @MyStevieTV</a>
					</div>
					<?php
						$showDownloads = false;
						//#showDownloads = $isLoggedIn;
						if ($showDownloads) {
							echo '<div id="getStevieTitle" class="bolder">';
							echo '<img class="tri" src="http://static.mystevie.com/png/website/tri.png" />';
							echo "Available Now";
							echo '</div>';
							echo '<div id="getStevieWin" class="rightBarItem">';
							echo '<a href="download"><img id="download_win_img" src = "http://static.mystevie.com/png/website/download_win.png" /></a>';
							echo '<div id="tryAlpha"><a href="download">Try the Alpha version of our new screen saver!</a></div>';
							echo '</div>';
						} 
					?>
					<div id="getStevieSoon" class="bolder">
						<img class="tri" src="http://static.mystevie.com/png/website/tri.png" />
						Coming Soon
					</div>
					<?php
/*						if (!$showDownloads) {
							echo '<div id="getStevieWin" class="rightBarItem">';
							echo '<img src = "http://static.mystevie.com/png/website/download_win.png" />';
							echo '</div>';
						} */
					?>
					<div id="getStevieAndroid" class="rightBarItem">
						<img src = "http://static.mystevie.com/png/website/download_android.png" />
					</div>
					<div id="getStevieiPhone" class="rightBarItem">
						<img src = "http://static.mystevie.com/png/website/download_iphone.png" />
					</div>
				</div> 
			</div>
			<div id="tv">
				<div id="glow"><img src = "http://static.mystevie.com/png/website/glow.png" /></div>
				<iframe id="embeddedSlideshow" name="" frameBorder="0" src="about:blank" scrolling="no"></iframe>
			</div>
			<div id="fixedBottomBarItems">
					<?php require('bottombar.php'); ?>
			</div>
		</div>
	</body>
</html>

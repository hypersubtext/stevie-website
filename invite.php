<?php 
	$pageTitle = "Invite your friends to Stevie";
	$pageurl = "invite";	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="https://www.facebook.com/2008/fbml">
	<head>
		<?php require('home/head.php'); ?>
		<script>
			function sendMessage() {
				$('#sendmail').submit();
			}
		</script>
	</head>
	<body onload="allLoaded();plantName();">
	  	<div id="fb-root"></div>
    	<script src="http://connect.facebook.net/en_US/all.js"></script>
    	
    	<script>
			FB.init({
				appId  : '210504105683030',
		        status : true,
		        cookie : true,
		        oauth: true
			});

			function sendRequestViaMultiFriendSelector() {
				FB.ui({method: 'apprequests',
				message: 'Join me and watch Stevie, a TV channel created from your Facebook feed!',
				filters:['app_non_users']				
				}, requestCallback);
			}
			      
			function requestCallback(response) {
				console.log(response);
			}

			function plantName() {
				$("#inviteBodyBox").prepend(settingsObj.user.name);
				$("#nameInvite").val(settingsObj.user.name);
				$("#senderemailInvite").val(settingsObj.user.email);
			}
		      
		</script>
    	
		<div id="whiteZone">
		</div>

		<div id="canvas">
			<div id="topBar">
				<?php require('home/topbar.php'); ?>
			</div>
			<div id="main">
				<div id="inviteFacebookDiv" onclick="sendRequestViaMultiFriendSelector();">
					<img src="http://static.mystevie.com/png/website/facebookinvite.png" /><br/>
					<span id="shareStevie">Share Stevie with your Facebook friends</span>
				</div>		
			<br/><br/>
			<div id="inviteBox">
				<form name="sendmail" id="sendmail" method="post" action="sendInvitation.php">
				<span class="bolder">Or enter your friends email addresses</span><br/>
				<input id="emailInvite1" class="emailInviteAddress" name="email1" type="text" /> <br/>
				<input id="emailInvite2" class="emailInviteAddress" name="email2" type="text" /> <br/>
				<input id="emailInvite3" class="emailInviteAddress" name="email3" type="text" /> <br/>
				<input id="emailInvite4" class="emailInviteAddress" name="email4" type="text" /> <br/>
				<input id="nameInvite" name="name" type="hidden" value="" />
				<input id="senderemailInvite" name="senderemail" type="hidden" value="" />
				<br/>
				<span class="bolder">Add your own words (optional)</span><br/>
				<textarea id="inviteBodyBox" name="body">
 has been watching Stevie, which turns your Facebook and Twitter into a super cool TV channel, 
and thought you should be watching your Stevie channel too!</textarea><br/>
				<div id="inviteSendButton" class="bolder"><a href="javascript: sendMessage();">Send</a></div>
				</form>
			</div>
			</div>
			<div id="fixedBottomBarItems">
				<?php require('home/bottombar.php'); ?>
			</div>
		</div>
	</body>
</html>

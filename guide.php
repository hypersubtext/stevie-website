<?php 
	$pageTitle = "Stevie Guide";
	$pageurl = "guide";
        $host = $_SERVER['HTTP_HOST'];
        $mytwin = str_replace("stevie", "mystevie", $host);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<?php require('home/head.php'); ?>
            <script type="text/javascript">
                WebFontConfig = {
                            monotype: {
                              projectId: '3b6f5a9f-d56c-4e16-9710-4dbe7d2a7c41'
                            }
                          };		      
			(function() {
		        var wf = document.createElement('script');
		        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
		            '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
		        wf.type = 'text/javascript';
		        wf.async = 'true';
		        var s = document.getElementsByTagName('script')[0];
		        s.parentNode.insertBefore(wf, s);
		      })();

                function visibleTitle () {
                    $("#pageSubtitle").css('visibility','visible');
                    $("#pageTitle").html(pageTitle);
                    drawTopBar();
                }
        </script>
        <style type="text/css">
            #fbtwImg {
                position: relative;
                top: 9px;
            }
            #pageSubtitle {
                visibility: hidden;
                position: relative;
                top: -40px;
            }
            #iframeGuide {
                position: relative; 
                top: -36px;
                width: 964px; 
                border: 0; 
                height: 4027px;
            }
            #allCats {
/*                padding-left: 48px;*/
            }
        </style>
        <link rel="stylesheet" type="text/css" href="/guide_resources/gallery.css">
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="/guide_resources/jMyCarousel.min.js"></script>
        <script type="text/javascript" src="/home/webjs.js"></script>
     	</head>
	<body onload="wipeLogo();">
	<div id="fb-root"></div>
	<script type="text/javascript">(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
    	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=210504105683030";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
        !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>		
		<div id="whiteZone">
		</div>

		<div id="canvas">
			<div id="topBar">
                            <?php require('home/topbar.php'); ?>
			</div>
			<div id="main">
                            <div id="pageSubtitle" style="padding-left:15px;">
                                <table cellpadding='0' cellspacing='0'  width='930'><tr><td>
                                All channels are based on content from
                                <img id="fbtwImg" src="http://cdn-guide.mystevie.com/website/fb_tw_yt_bright.png" alt="Facebook, Twitter and Youtube"/>
                                </td><td class="pressLink" align="right" style='padding-top: 10px;'>
                                Missing a channel? <a href="contact">Send us feedback</a>
                                </td></tr></table>
                            </div>
	
                            <div id="rightBar">
                                <div id="likeFollow">
                                    <!-- <div class="fb-like-box" data-href="https://www.facebook.com/MyStevieTV" data-width="200" data-show-faces="true" data-border-color='white' data-stream="false" data-header="false" style='background-color: white; margin-top: -5px'></div> -->
                                    <div class="fb-like" data-href="https://www.facebook.com/MyStevieTV" data-send="false" data-width="180" data-show-faces="false"></div>
                                    <a href="https://twitter.com/MyStevieTV" class="twitter-follow-button" data-show-count="true">Follow @MyStevieTV</a>
                                </div>
                                <div id="twitter_div"></div>
                                <div style='margin-top: 25px; font-size: 17px; width: 189px; margin-left: -13px;font-family:arial;font-size:12px'>
                                    Chosen to participate in:<br>
                                    <a href='http://www.microsoftrnd.co.il/strategic-partnerships/microsoft-accelerator-for-windows-azure' target="_blank">
                                        <img src='http://static.mystevie.com/png/website/MicrosoftAccelerator_white.png' style='padding-top: 3px;margin-left: 0px; width: 150px;'>
                                    </a>
                                </div>              
                                <div style='padding-top: 20px; font-size: 17px; width: 189px; margin-left: -13px;'>
                                    <a href='http://techcrunch.com/2012/05/21/stevie-social-tv-launch/' target="_blank"><img src='http://static.mystevie.com/png/website/disrupt_badge.png' style='padding-top: 0px'></a>
                                </div>
                                <div style='padding-top: 20px; font-size: 17px; width: 189px; margin-left: -13px;'>
                                    <a href='http://blog.mipworld.com/2013/04/liveblog-mipcube-innovation-winners-2013-revealed/' target="_blank"><img src='http://static.mystevie.com/png/website/mipcube_badge.png' style='padding-top: 0px'></a>
                                </div>
                            </div> 
            <div id="allCats">
<?php
    $staticurl = 'http://cdn-guide.mystevie.com/';
    $json_code = file_get_contents('http://' . $mytwin . '/server/services/guide_service.php?command=get_guide_channels');
    $cats = json_decode($json_code)->description;
    $catsnum = count($cats);
    
    for($catid=0; $catid<$catsnum; $catid++) {
        $channels = $cats[$catid]->channels;
        $category = $cats[$catid]->category;
        $channum = count($channels);
        
        drawCat (landingPage($category->url), $catid, $category->style->guide_image, $category->style->name);

    }

function arrowNum($c) {
    return ($c%5)+1;
}
                
function drawChan ($url, $chKey, $guide_image, $name) {
    // foreach ($chKey in catObj.channels) {        
    global $staticurl;
             crlf('<li class="snakeListItem"><a href="' . $url . '">');
             crlf('<img class="snakeImg" src="' . $guide_image . '">');
             crlf('<img class="snakePlayImg" src="' . $staticurl . 'arrows/' . arrowNum($chKey) . '.png">');
             crlf('<div class="snakeChannelTitleBox noLink">');
             crlf('<span class="snakeChannelTitle noLink">');
             crlf($name);
             crlf('</span>');
             crlf('</div>');
             crlf('</a>');
             crlf('</li>');
         //}
}                  
                
function drawCat ($url, $key, $guide_image, $name) {
         global $staticurl, $channels, $channum;
         crlf('<div id="snake' . $key . '" class="snake noLink">');
         crlf('<div class="leadCat noLink">');
         crlf('<a href="' . $url . '"><img class="leadCatImg" src="' . $guide_image . '">');
         crlf('<img class="leadCatPlayImg" src="' . $staticurl . 'arrows/0.png">');
         crlf('<div class="leadCatPlayLabel">PLAY</div>');
         crlf( '</a>');
         crlf('<div id="lead' . $key . '" class="leadCatTitle">' . $name . '</div>');
         crlf('</div>');
         crlf('<div id="Cat' . $key . '" class="jMyCarousel jMyColors">');
         crlf('<ul style="list-style-type: none;">'); 
         for($chanid=0; $chanid<$channum; $chanid++) {
             drawChan (landingPage($channels[$chanid]->url),$chanid, $channels[$chanid]->style->guide_image, $channels[$chanid]->style->name);
         }
         crlf('</ul>');
         crlf('</div>');
         crlf('</div>');
         
 }

 function landingPage ($st) {
     $newst =  str_replace("mystevie", "stevie", $st);
     $newst =  str_replace("backend.stevie.com/on/", "www.stevie.com", $newst);
     return str_replace('com//', 'com/', $newst);
 }
 
 function crlf ($st) {
     echo $st . "\r\n";
 }
 
    
 
 ?>

            </div>
        </div>
        <div id="fixedBottomBarItems">
                <?php require('home/bottombar.php'); ?>
        </div>
    </div>
            <script type="text/javascript">
                function showCats () {
                    $("#allCats").css('visibility', 'visible');
                    catsNum = <?php echo $catsnum; ?>;
                    for (var i=0;i<catsNum;i++) {
                        if ($("#lead" + i).height()>15) {
                            $("#lead" + i).css("bottom",($("#lead" + i).height()+20) + "px");
                        };
                        $("#Cat" + i).jMyCarousel({visible: '700px',speed: 300, step: 50,easing: 'linear'}); 
                    }
                }
                    $("#logo2").html('<img src="http://static.mystevie.com/png/website/logo/logo2.png"/>');
                    $("#canvas").show();
                    showWhiteZone();
                    showCats();
                    setTimeout(function(){visibleTitle();},1000);
            </script>
	</body>
</html>

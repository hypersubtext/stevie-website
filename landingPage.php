<?php 
    
    require_once($_SERVER['DOCUMENT_ROOT'] . "/lib/domains.php");

    $pageType = 'landingPage';
    $pageTitle = '';
    $pageurl = '';

    function getVideoEmbedMetaTags($videoClip) {
        global $clipImage, $embedUrlToMetaTags;
        $clipImage = $videoClip->picture;
        if($videoClip->source == "YouTube") {
            $embedUrlToMetaTags = "http://www.youtube.com/v/" . $videoClip->source_id . "?version=3&amp;autohide=1";
        }
        if($videoClip->source  == "Dailymotion") {
            $embedUrlToMetaTags = "http://www.dailymotion.com/swf/video/" . $videoClip->source_id . "?autoPlay=1";
        }
        if($videoClip->source  == "Vimeo") {
            $embedUrlToMetaTags = "http://vimeo.com/moogaloop.swf?clip_id=" . $videoClip->source_id;
        }
    }

    function getChannel($channelId) {
        $reqURL = CLIENT_PATH . '/server/services/search_service.class.php?command=get_by_url_suffix&url_suffix=' . urlencode($channelId) . '&related=1';
        if(isset($_GET['v'])) {
            $reqURL.="&videoId=".$_GET['v'];
        };
        $channelDump = file_get_contents($reqURL);
        $channelJSON = json_decode($channelDump);
        return $channelJSON;
    }

    
    function strip25 ($st) {
        return str_replace("%2523", "%23", $st);
    }

    function enableSlash ($st) {
        return str_replace('%2F','/',$st);

    }
    
    $channelName = "";
    $channelImage = "";
    $channelDescription = "";
    $channelKeywords = "";

    $uri = $_SERVER["REQUEST_URI"];

    // get channelId from ugly or pretty url
    if ((substr($uri,1,11)=="landingPage")||(substr($uri,1,11)=="landingpage")) {
        $requestedChannelId = $_GET["channelId"];
    } else {
        $requestedChannelId = substr($uri,1);
    }
    
    $requestedChannelId = str_replace('#','%23',$requestedChannelId);
    
    $questionMarkPos = strpos($requestedChannelId, "?");
    if ($questionMarkPos !== false) {
        $requestedChannelId = substr($requestedChannelId, 0, $questionMarkPos);
    }
    
    $ampersandMarkPos = strpos($requestedChannelId, "&");
    if ($ampersandMarkPos !== false) {
        $requestedChannelId = substr($requestedChannelId, 0, $ampersandMarkPos);
    }

    $pageurl = $requestedChannelId;
    $embedUrlToMetaTags = '';
    $clipImage = '';

    if ($requestedChannelId != 'guidefirst') {
        $channelDetails = getChannel($requestedChannelId);
    } else { // if the landing page is guidefirst
        $channel = '';
        $channelName = 'Stevie Channnels';
        $channelDisplayName = 'Stevie Channels';
        $channelImage = 'http://static.mystevie.com/png/website/stevie_home_banner.png';
        $channelDescription = 'Facebook & Twitter turn into TV channels: Watch a lean back experience made of everything you and your friends share, popular hashtags and topics, viral clips and hundreds of official social network pages';
        $channelKeywords = 'Stevie,channel,fantastic';   
        $requestedChannelId = '';    
        $pageTitle = 'Stevie Channnels';
        $related = array();
    } 
    
    if (isset($channelDetails->description)) {
        $channel = $channelDetails->description->channel;
        $channelName = $channel->style->name;
        $channelDisplayName = str_replace('%23', '#', $channelName);
        $channelImage = $channel->style->webclient->vodIcon;
        $channelDescription = $channel->description;
        $channelKeywords = $channel->keywords;
        $pageTitle = $channelDisplayName." Channel on Stevie. Transforming the social web into beautiful TV";
        $related = $channelDetails->description->related;
        if (isset($channelDetails->description->video)) {
            $videoClip = $channelDetails->description->video;
            getVideoEmbedMetaTags($videoClip);
        }
    } else { // error or no-channel, redirecting
        // header( 'Location: http://www.stevie.com' );
    }
   
?>

<!DOCTYPE HTML>
<html>
    <head>
        <?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/head.php'); ?>
        <script type="text/javascript" src="/lib/script/search_widget.js"></script>
        <?php
            // redirect to pretty url after error.php
            if (substr($uri,1,11)=="landingPage") {
                $channelPrettyKeyword = $requestedChannelId;
                if (substr($channelPrettyKeyword, 0, 3)=="%23") {
                    $channelPrettyKeyword = "%25" . substr($channelPrettyKeyword,1);
                }
                echo "<script type='text/javascript'>";
                echo "window.history.replaceState('','', '/" . $channelPrettyKeyword . "');";
                echo "</script>";
            }
        ?>        
    </head>
    <body class="room">
        <div id="page" class="<?= $pageurl ?>">
            <?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/top_nav.php'); ?>
            <div id="main" class="container">
                <div id="stage" class="col-md-9">
                    <div id="channel_search">
                        <div class="input-group input-group-lg">
                            <input id="search_input" type="text" placeholder="What would you like to watch?" class="form-control">
                            <span class="input-group-btn"><button class="btn btn-default" onclick="searchWidget.requestChannelFromInputVal();" type="button">Go!</button></span>
                        </div>
                    <ul id="search_results"></ul>
                    </div>

                    <div id="stevie_embed">
                        <iframe src="<?= CLIENT_PATH ?>/embed/<?= enableSlash(urlencode($requestedChannelId)) ?>?autoplay=yes&muted=no&color_scheme=transparent<?php if(isset($_GET['v'])) { echo "&v=".$_GET['v'];}; ?>" id="stvplayer" allowfullscreen  allowtransparency="true" ></iframe>
                    </div>
                    <div id="disclaimer">
                        All channels are automatically created from Twitter, Facebook<br> 
                        and YouTube and are not affiliated with or endorsed by their owners.
                    </div>
                </div>

                <div class="col-md-3" id="sidebar">
                    <div id="channel_info">
                        <span>
                            <h2 id="channel_title"><?php echo $channelDisplayName ?></h2>
                            <div id="desc"><?php echo $description ?></div>
                        </span>
                        <div id="expand_channel_info">&#x25BE; Read More...</div>
                        <div id="contract_channel_info">&#x25B4; Read Less...</div>
                    </div>

                        <div class="dl_buttons landing_page">
                            <h4><a target="_blank" href="https://play.google.com/store/apps/details?id=com.stevie">Get Stevie for Chromecast</a>, <a target="_blank" href="http://itunes.apple.com/us/app/stevie/id547231007">Apple TV</a> and <a target="_blank" href="https://play.google.com/store/apps/details?id=com.stevie">Android TV</a></h4>
                            <a class="dl_button" id="google_dl_button" href="https://play.google.com/store/apps/details?id=com.stevie">
                                <img alt="Get it on Google Play" src="http://cdn.mystevie.com/png/website/channelPages/GooglePlayButton.png" />
                            </a>
                            <a class="dl_button" id="apple_dl_button" href="http://itunes.apple.com/us/app/stevie/id547231007">
                                <img class="appstore" src="http://cdn.mystevie.com/png/website/channelPages/AppStoreButton.png" />
                            </a>
                        </div>

                </div>
                    
                <?php
                    if (count($related) > 0) {
                        echo '<div id="related_channels" class="col-md-9"><div class="row">';
                        foreach ($related as $relatedChannel) {
                            echo '<div class="col-sm-2 col-xs-4 related_channel"><a href="' . $relatedChannel->landing_page_url  . '" title="' . $relatedChannel->style->name . '">
                            <img src="' . strip25($relatedChannel->style->webclient->vodIcon) . '" /><span></span><div class="gradient"><h5 class="sans">' . $relatedChannel->style->name . '</h5></div></a></div>';
                        }
                    echo '</div></div><a class="link_to_guide" href="http://stevie.com/guidefirst">Watch More Channels &raquo;</a>';
                    }
                ?>
            </div>  <!-- main -->   
        </div> <!-- page -->
        <div id="footer">
            <?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/footer.php'); ?>
        </div>
    </body>
</html>



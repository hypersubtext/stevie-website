$(document).ready(function() {
	var myHost = document.domain;
	var twinHost = myHost.replace('stevie', 'mystevie');
	var clientPath;
	
	if(twinHost.indexOf("www") !=-1 ) {
		clientPath = 'http://' + twinHost + '/on/';
	} else  {
		clientPath = 'http://' + twinHost + '/';
	}

//        var clientPath = "http://"+location.host+"/";
                var host = clientPath;



	var servicesPath = clientPath+"server/services/";
	var clientArgs = "&component=website&platform=web";

	var errorCb = function(res) {
		console.error("Server error",res)
	}

	function doServerRequest(req,cb) {
		$.getJSON(servicesPath+req+clientArgs,function(res) {
			if(!res.status || !res.description) errorCb(res);
			if(res.status!=200) errorCb(res);
			cb(res.description);
		});
	}

	function drawTrendingTopics(topics) {
		$("#topics").html("");
		for(var i in topics) {
			var channel = topics[i];
			var channelElem = $('<div class="topic channel">'+channel.pretty_code+'</div>');
			$(channelElem).data("channel",channel)
			channelElem.click(function() {
				requestClientProgram($(this).data("channel"));
				// updateTitle();
			});
			$("#topics").append(channelElem);
		}
		// Random background colors for channel squares
		$(".topic").each(function() {
			// console.log($(this));
			$(this).css( "background-color", function() {
				colors = ['#f21c62', '#46c0ff', '#29b962', '#f5cf00' , '#9b74cd' , '#a0a0a0']
				return colors[Math.floor(Math.random()*colors.length)];
			});
		});
	}



    function requestClientProgram(channel) {
    	
    	var pckg = {"cmd":"requestVODChannel",
    				"channelCode": channel.programs[0].code,
    			    "extraDetails":channel.programs[0].default_extra_details};
    	$("#client")[0].contentWindow.postMessage(pckg,"*");

        console.log("channel change",channel);
    }
    

    doServerRequest("trending_service.class.php?command=get",drawTrendingTopics);
    $("#client").attr("src",host+"/trending");
    
    $("#trendingButton").click(function() {
        $("#client").attr("src",host+"/trending");
    })
    
    setInterval(function() {
        doServerRequest("trending_service.class.php?command=get",drawTrendingTopics);
    },5000*60);

    function getEmbedHeight() {
		var embedHeight = ($("#stevie_embed").width() / 16 * 9) + "px";
		$("#stevie_embed").css("height" , embedHeight);
	}

	getEmbedHeight();
	$( window ).resize(function() {
		getEmbedHeight();
	});

	var messageEventHandler = function(e){
         var packet = e.data;  
         console.log("client recieved packet",e.data)
         if(packet.nowPlaying) {
             $("#main h3 span").text(packet.nowPlaying)
         }
    }
    window.addEventListener('message', messageEventHandler,false);


    $("head").append('<script src="'+clientPath+'/web_common/search.js"></script>')

    setTimeout(function() {
    var searchWidget = new Search(
    	clientPath,
    	$("#search_input"), // input dom (jQuery plz)
    	$("#search_results"), // results dom
    	"website", // website or client
    	function(channel) {
    	var pckg = {"cmd":"requestVODChannel",
    				"channelCode": channel.code,
    			    "extraDetails":channel.extra_details};
    	$("#client")[0].contentWindow.postMessage(pckg,"*");    	
    	}, // callback function for ordering a channel
    	true, // autocomplete?
    	function finishedResults() {} // called when results are populated
    	);
	},500)

});


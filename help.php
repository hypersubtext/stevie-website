<?php 
    
    require_once($_SERVER['DOCUMENT_ROOT'] . "/lib/domains.php");

    $pageType = 'textPage';
    $pageTitle = 'Help';
    $pageurl = "help";

	if (isset($_GET["platform"])) {
		$currentPlatform = $_GET["platform"];
                if (substr( $currentPlatform, 0, 3 ) === "iOS") {
                    $currentPlatform = "ios";
                }
	}
?>

<!DOCTYPE HTML>
<html>
	<head>
        <?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/head.php'); ?>
        <?php
		?>
		<script type="text/javascript">
			// make settings page layout responsive for devices
			var myPlatform = '<? echo strtolower($currentPlatform); ?>';
			$(document).ready(function() {
				$("body").addClass(myPlatform);
	
				if (myPlatform=='android') {
					$.get( "androidHelp.html", function( data ) {
						$("body").html(data);
						$("body").css('visibility' , 'visible').hide().fadeIn('fast');
					});
				}
				else if (myPlatform=='ios') {
					$.get( "iosHelp.html", function( data ) {
						$("body").html(data);
						$("body").css('visibility' , 'visible').hide().fadeIn('fast');
					});
				}
			});
		</script>

		<style type="text/css">
			body.android, body.ios { background: #212121; width: 100%; height: 100%; visibility: hidden; }
		</style>

	</head>
	<body class="room">
		<div id="page" class="<?= $pageurl ?>">
			<?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/top_nav.php'); ?>
			<div id="main" class="container">
				<div class="panel">
					<h1 id="<?= $pageurl ?>" style="background-image : url(lib/style/<?= $pageurl ?>_title.png)"><?= $pageTitle ?></h1>
					<h2>Frequently Asked Questions</h2>
					<ul id="faq_list">
						<li><a href="#A">What is Stevie?</a></li>
						<li><a href="#B">Where can I watch my Stevie?</a></li>
						<li><a href="#C">Is there any way to interact with Stevie?</a></li>
						<li><a href="#D">Which content sources can be mixed in Stevie?</a></li>
						<li><a href="#E">Do I need to have a Facebook account in order to watch my Stevie?</a></li>
						<li><a href="#F">How do I upload a clip?</a></li>
					</ul>
					<div id="faq_answers">
						<div id="A">
							<h3>What is Stevie?</h3>
							<p>
								Stevie turns your personal social feeds (like Facebook and Twitter) into your own broadcast television experience. When your hands are busy or you can't sit at the computer, but still want to be entertained and enjoy all the content your friends are sharing - just tune in to Stevie!
								Watch videos, status messages, events, photos, birthdays and more on your computer or your connected TV screen.
								Stevie not only shows you content from friends, but turns your feed into shows such as The Comedy Strip or Music Non Stop, and news bulletins like Top Stories or New Albums for a great TV viewing experience. It also brings you new and exciting content you might have missed from friends of friends, and top hits from around the web.
							</p>
						</div>

						<div id="B">
							<h3>Where can I watch my Stevie?</h3>
							<p>
								On your iPad: Download <a href='http://itunes.apple.com/us/app/stevie/id547231007'>Stevie for iPad</a> from the AppStore, connect to your Facebook and start watching.<br/>
								On Windows 8: Download <a href='http://apps.microsoft.com/webpdp/app/stevie/91c6b9c1-f436-4889-9f7e-e9f18e254089'>from the Windows 8 Store</a>, connect and watch your Stevie instantly.<br/>
								Stevie is also available here, on the web - <a href='/'>www.stevie.com</a> - Just click "Start  your Stevie" to connect using your Facebook and start your Stevie.<br />
								Once connected, don’t forget to go full screen by clicking F11.<br />
							</p>
						</div>

						<div id="C">
							<h3>Is there any way to interact with Stevie?</h3>
							<p>
								Of course there is! Here’s how you can control Stevie from your keyboard:
								<img src="http://static.mystevie.com/png/helplayer.png" style='margin: 5px'/>
								You can also download our Smart Remote onto your <a href='http://itunes.apple.com/us/app/stevie-remote/id524963172'>iPhone</a>
								or <a href='http://play.google.com/store/apps/details?id=com.phonegap.StevieRemote'>Android</a> 
								phone, where you’ll have many more interaction options, including - 
								Skip to a specific show or video, share videos, go to the original video post on Facebook or Twitter - And replay any clip you ever watched on Stevie.
							</p>
						</div>

						<div id="D">
							<h3>Which content sources can be mixed in Stevie?</h3>
							<p>
								Add content from your Facebook and Twitter account - more sources coming soon. Which networks would you like to see added? <a href="../contact">Let us know!</a>
							</p>
						</div>

						<div id="E">
							<h3>Do I need to have a Facebook account in order to watch my Stevie?</h3>
							<p>
								Right now connecting to a Facebook account is the only way to register to Stevie. We will add more options in the near future.
							</p>
						</div>

						<div id="F">
							<h3>How do I upload a clip?</h3>
							<p>
								You don't need to upload anything to Stevie. Stevie automatically fetches video, images and other content from your Facebook and Twitter accounts.
								So if you want to watch a clip on Stevie - Just share it on Facebook or Twitter :)
							</p>
						</div>

					</div>
				</div> <!-- panel -->
			</div>	<!-- main -->	
		</div> <!-- page -->
	    <div id="footer">
			<?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/footer.php'); ?>
		</div>
	</body>
</html>


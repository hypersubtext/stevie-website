<?php 
    
    require_once($_SERVER['DOCUMENT_ROOT'] . "/lib/domains.php");

    $pageType = 'textPage';
    $pageTitle = 'Press';
    $pageurl = "press";
?>

<!DOCTYPE HTML>
<html>
	<head>
        <?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/head.php'); ?>
	</head>
	<body class="room">
		<div id="page" class="<?= $pageurl ?>">
			<?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/top_nav.php'); ?>
			<div id="main" class="container">
				<div class="panel">
					<h1 id="<?= $pageurl ?>" style="background-image : url(lib/style/<?= $pageurl ?>_title.png)"><?= $pageTitle ?></h1>
					<div class="row">
						<div id="press_menu" class="col-md-4 col-md-push-8">
							<div>
								<h5>Contact Us!</h5>
								<a href='mailto:press@stevie.com'>press@stevie.com</a>
							</div>
							<iframe style="padding-left:10px;" id="this_is_stevie" width="352" height="198" src="//www.youtube.com/embed/PIM0N8KXKxQ" frameborder="0" allowfullscreen></iframe>
							<ul>
								<li><a href="about">About Us</a></li>
								<li><a href="about#company_overview">Company Overview</a></li>
								<li><a href="about#team">Meet the Team</a></li>
								<li><a href="products">Stevie Products</a></li>
								<li><a href="SteviePressKit.pdf">Stevie Press Kit (PDF)</a></li>
								<li><a href="StevieScreenshots.zip">Download Stevie Screenshots</a></li>
								<li><a href="CelebTViPad.zip">Download Celebrity Tweet TV Screenshots</a></li>
							</ul>
						</div>
						<div id="press_main" class="col-md-8 col-md-pull-4">
							<h5>Stevie turns social media feeds into a beautiful broadcast TV experience, creating entertainment that's personal &amp; social across platforms.</h5>
							<ul id="articles">
                                                            <?php
                                                            
                                                                function printItem ($headline, $date, $outcode, $author, $url) {
                                                                    switch ($outcode) {
                                                                        case "btv":
                                                                            $icon = "btv.png";
                                                                            $outlet = "Broadband TV news";
                                                                            break;
                                                                        case "tlr":
                                                                            $icon = "lostremote_logo.png";
                                                                            $outlet = "The Lost Remote";
                                                                            break;
                                                                        case "go":
                                                                            $icon = "gigaom_logo.png";
                                                                            $outlet = "Giga OM";
                                                                            break;
                                                                        case "tnw":
                                                                            $icon = "tnw_logo.png";
                                                                            $outlet = "The Next Web";
                                                                            break;
                                                                        case "fo":
                                                                            $icon = "fo.jpg";
                                                                            $outlet = "Forbes";
                                                                            break;
                                                                        case "pw":
                                                                            $icon = "pr.jpg";
                                                                            $outlet = "PR News Wire";
                                                                            break;
                                                                        case "21":
                                                                            $icon = "21c.jpg";
                                                                            $outlet = "Israel21c";
                                                                            break;
                                                                        case "st":
                                                                            $icon = "st.jpg";
                                                                            $outlet = "SocialTimes";
                                                                            break;
                                                                        case "ex":
                                                                            $icon = "ex.jpg";
                                                                            $outlet = "Examiner";
                                                                            break;
                                                                        case "amf":
                                                                            $icon = "amf.jpg";
                                                                            $outlet = "All My Faves";
                                                                            break;
                                                                        case "tc":
                                                                            $icon = "tc.jpg";
                                                                            $outlet = "TechCrunch";
                                                                            break; 
                                                                        case "bk":
                                                                            $icon = "bk.jpg";
                                                                            $outlet = "BetaKit";
                                                                            break; 
                                                                        case "hp":
                                                                            $icon = "huff.jpg";
                                                                            $outlet = "Huffington Post";
                                                                            break; 
                                                                        case "sb":
                                                                            $icon = "socialmediabiz.gif";
                                                                            $outlet = "Socialmedia.biz";
                                                                            break; 
                                                                        case "gl":
                                                                            $icon = "globes.jpg";
                                                                            $outlet = "Globes";
                                                                            break; 
                                                                    }
                                                                    echo '<li>';
                                                                    echo '<a target="_blank" href="' . $url . '">';
                                                                    echo '<img src="http://static.mystevie.com/png/website/press/' . $icon . '" alt="' . $outlet . '"/>';
                                                                    echo '<p>' . $headline . '</p>';
                                                                    echo '<i>' . $date . ' - ' . $outlet . ' - ' . $author . '</i>';
                                                                    echo '</a>';
                                                                    echo '</li>';
                                                                }
                                                            
                                                                printItem ("Stevie brings social channels to Chromecast and Android TV", "June 25, 2014", "btv", "Robert Briel", "http://www.broadbandtvnews.com/2014/06/25/stevie-brings-social-channels-to-chromecast-and-android-tv/");
                                                                printItem ("With Chromecast Update, Stevie is One Step Closer to Bridging the Social and TV Divide", "June 25, 2014", "tlr",  "Natan Edelsburg", "http://lostremote.com/with-chromecast-update-stevie-is-one-step-closer-to-bridging-the-social-and-tv-divide_b44871");
                                                                printItem ("Stevie, the MTV of social video curation, adds Chromecast support to its mobile apps", "June 24, 2014", "go", "Janko Roettgers", "http://gigaom.com/2014/06/24/stevie-the-mtv-of-social-video-curation-adds-chromecast-support-to-its-mobile-apps/");
                                                                printItem ("Stevie now turns any Twitter topic into a social TV channel", "December 23, 2013", "tnw", "Paul Sawers", "http://thenextweb.com/apps/2013/12/23/stevie-now-turns-twitter-topic-social-tv-channel");
                                                                printItem ("10 Female Founders To Watch Out Of Israel", "December 23, 2013", "fo", "Shuly Galili", "http://www.forbes.com/sites/women2/2013/12/23/10-female-founders-to-watch-out-of-israel");
                                                                printItem ("The Social Television Experience With Stevie", "November 22, 2013", "fo", "Rakesh Sharma", "http://www.forbes.com/sites/rakeshsharma/2013/11/22/the-social-television-experience-with-stevie/");
                                                                printItem ("How 'Stevie' is curating celebrities’ social channels into TV style content","October 15, 2013", "tlr", "Natan Edelsburg","http://lostremote.com/how-stevie-is-curating-celebrities-social-channels-into-tv-style-content_b38982");
                                                                printItem ("New iOS app turns Kim Kardashian’s tweets into a TV channel", "October 11, 2013", "go", "Janko Roettgers", "http://gigaom.com/2013/10/11/new-ios-app-turns-kim-kardashians-tweets-into-a-tv-channel/");
                                                                printItem ("Tune in to Louis C.K And Rihanna's Point Of View 24/7 on Celebrity Tweet TV from Stevie", "October 8, 2013", "pw", "Press Release", "http://ca.finance.yahoo.com/news/tune-louis-c-k-rihannas-203000323.html");
                                                                printItem ("Stevie tailors TV and online content to you", "October 3, 2013", "21", "Karin Kloosterman", "http://israel21c.org/culture/stevie-tailors-tv-and-online-content-to-you/");
                                                                printItem ("Exclusive: Stevie Launches Pet-Themed Channel, 'All Your Cats Are Belong to Us'", "July 12, 2013", "st", "Devon Glenn", "https://socialtimes.com/exclusive-stevie-launches-pet-themed-channel-all-your-cats-are-belong-to-us_b131790");
                                                                printItem ("tevie scoops the MIPCube Lab innovation contest", "April 10, 2013", "tnw", "Paul Sawers", "http://thenextweb.com/events/2013/04/10/mipcube/");
                                                                printItem ("Will the future of broadcast TV involve social media and Stevie?", "April 6, 2013", "ex", "Tradina Demary", "http://www.examiner.com/article/will-the-future-of-broadcast-tv-involve-social-media-and-stevie");
                                                                printItem ("Turn Your Facebook Profile Or Page Into A TV Show With Stevie", "March 28, 2013", "st", "Megan O'Neill", "http://socialtimes.com/turn-your-facebook-profile-or-page-into-a-tv-show-with-stevie_b122740");
                                                                printItem ("Stevie leans back and moves forward, launching more than 450 channels on social TV platform", "March 28, 2013", "tnw", "Paul Sawers", "http://thenextweb.com/apps/2013/03/28/stevie-social-tv-service/");
                                                                printItem ("Stevie TV: Watch Facebook and Twitter come to life on screen", "January 14, 2013", "amf", "Brooke Weinbaum", "http://www.allmyfaves.com/blog/weekly-faves/stevie-tv-watch-facebook-and-twitter-come-to-life-on-screen/");
                                                                printItem ("Social TV Platform Stevie Inks $1.5M In Funding, Launches Cool iOS App", "December 19, 2012", "tc", "Mike Butcher", "http://techcrunch.com/2012/12/19/social-tv-platform-stevie-inks-1-5m-in-new-funding-launches-cool-ios-app-to-turn-social-feeds-into-tv-channels/");
                                                                printItem ("Leanback TV app Stevie releases iPhone version, raises $1.5M", "December 19, 2012", "go", "Janko Roettgers", "http://gigaom.com/video/stevie-iphone-app-series-a/");
                                                                printItem ("Stevie brings its unique take on social TV to the iPhone and grabs $1.5m funding", "December 19, 2012", "tnw", "Martin Bryant", "http://thenextweb.com/insider/2012/12/19/stevie-brings-its-unique-take-on-social-tv-to-the-iphone-and-grabs-1-5m-funding-from-horizons-ventures/");
                                                                printItem ("Stevie raises additional $1.5M in funding and expands to iPhone", "December 19, 2012", "tlr", "Natan Edelsburg", "http://lostremote.com/stevie-raises-additional-1-5m-in-funding-and-expands-to-iphone_b35696");
                                                                printItem ("Stevie iPhone App Turns Your Twitter And Facebook Feeds Into A TV Show", "December 19, 2012", "st", "Megan O'Neill", "http://socialtimes.com/stevie-iphone_b113872");
                                                                printItem ("Stevie Partners With Microsoft to Bring Social TV to Xbox's 70 Million Users", "October 31, 2012", "bk", "Chantal Da Silva", "http://betakit.com/2012/10/31/stevie-partners-with-microsoft-to-bring-social-tv-to-xboxs-70-million-users");
                                                                printItem ("Bringing your social graph to TV, meet Stevie", "October 31, 2012", "tlr", "Natan Edelsburg", "http://lostremote.com/bringing-your-social-graph-to-tv-meet-stevie_b34780");
                                                                printItem ("Social TV service Stevie lands on the Xbox, launches iPad app for the US election", "October 31, 2012", "tnw", "Anna Heim", "http://techcrunch.com/2012/05/21/stevie-social-tv-launch/");
                                                                printItem ("Social TV Platform Stevie Launches Election 2012 Channel Programmed By Tweets & Facebook", "October 31, 2012", "st", "Megan O'Neill", "http://socialtimes.com/stevie-election_b109063");                                                                
                                                                printItem ("Stevie brings MTV-style social video curation to the iPad", "September 24, 2012", "go", "Janko Roettgers", "http://gigaom.com/video/stevie-ipad-app/");
                                                                printItem ("Meet the three coolest companies from Microsoft’s Azure accelerator in Israel", "September 6, 2012", "tnw", "Alex Wilhelm", "http://thenextweb.com/insider/2012/09/06/meet-three-coolest-companies-microsofts-azure-accelerator-israel/");
                                                                printItem ("Stevie Brings the Social Web to the Comfort of Your Couch", "August 27, 2012", "hp", "Hillel Fuld", "http://www.huffingtonpost.com/hillel-fuld/stevie-brings-the-social-web_b_1831086.html");
                                                                printItem ("'Social TV' just got a whole new meaning with Stevie", "June 6, 2012", "sb", "Ayelet Noff", "http://www.socialmedia.biz/2012/06/06/stevie-the-term-social-tv-just-got-a-whole-new-meaning/");
                                                                printItem ("Social TV co Stevie creates content from Facebook", "May 22, 2012", "gl", "Roy Goldenberg", "http://www.globes.co.il/serveen/globes/docview.asp?did=1000750934");
                                                                printItem ("Stevie Turns Your Social Feeds Into TV Shows", "May 21, 2012", "tc", "Anthony Ha", "http://techcrunch.com/2012/05/21/stevie-social-tv-launch/");
                                                            ?>
							</ul>
						</div>
					</div>
				</div> <!-- panel -->
			</div> <!-- main container -->
		</div> <!-- page -->
	    <div id="footer">
			<?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/footer.php'); ?>
		</div>
	</body>
</html>



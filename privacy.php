<?php 
    
    require_once($_SERVER['DOCUMENT_ROOT'] . "/lib/domains.php");

    $pageType = 'textPage';
    $pageTitle = 'Privacy Policy';
    $pageurl = "privacy";
?>

<!DOCTYPE HTML>
<html>
	<head>
        <?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/head.php'); ?>
	</head>
	<body class="room">
		<div id="page" class="<?= $pageurl ?>">
			<?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/top_nav.php'); ?>
			<div id="main" class="container">
				<div class="panel">
					<h1 id="<?= $pageurl ?>" style="background-image : url(lib/style/<?= $pageurl ?>_title.png)"><?= $pageTitle ?></h1>
					<i>Last Revised January 1st, 2012.</i>
					<p>We, at stevie.com value your privacy and the privacy of your friends and therefore have set up this Privacy Policy in order to notify you how we process your personal information, what personal information we save and who else has access to your information.</p>
					<h5>1. What personal information do we retain?</h5>
					<p>We retain some of your personally identifiable information following your authorization and log-in through a 3rd party social account (such as Facebook or Twitter), including your user name, email address, Facebook ID or Twitter ID, your friend list, your profile photo and other personal information you specifically sent us.</p>
					<h5>2. What non-personal information do we retain?</h5>
					<p>We also retain some non-personal information; we cache the information which is transferred through our service for a limited time, and we retain a list of all your watched videos so we can show you better quality &amp; more relevant videos, a list of the friends you decided to block through the service, pages and videos you shared through the service and other machines or platforms you authorized Stevie on (such as your mobile device).  Moreover, we collect and process some information which relates directly to your use of Stevie, such as the time spent on service, your browser, operating system, time zone and other non-personal information. We may use cookies to authenticate your login and to process your behavior on the service.</p>
					<h5>3. What information don’t we store:</h5>
					<p>We do not store permanently any information regarding the content of your statuses, your messages, your contact list’s names or any sensitive information such as your sexual preferences, your religion, any trade-union affiliation or medical information. Moreover, we do not retain your payment information, credit card and other similar data.</p>
					<h5>4. How do we process your information?</h5>
					<p>We collect your personal information through your social network’s application program interface (API) and cache it in order to present it to you in a  televised manner; we do not keep it permanently and we only retain the list of your watched videos in order to keep track of what have you seen already.  When you use our services to like or share other users’ content, we refer you to your social networks and may process and send them some information regarding the content you liked.</p>
					<h5>5. Who has access to your information?</h5>
					<p>Apart from our servers and automated services (our staff may not see your personal information unless you specifically ask so), some information is transferred to your social networks or friends on social networks if you specifically asked for it. Moreover, we may use some information in an aggregated manner to provide you with better service (such as showing you the number of people who watched the video) or advertisements.</p>
					<h5>6. Why do we retain a list of your watched videos?</h5>
					<p>We keep a list of your watched videos in order to provide you with better service, to avoid too much repetition of videos and to learn what you’d like to watch. This list is encrypted on our servers, disengaging a direct link between the user and their data. You can delete this list at any time through your preferences.</p>
					<h5>7. 3rd Party Services &amp; Processing:</h5>
					<p>We use the services of 3rd parties, including analytical services, authentication services and others such as Google Analytics and Facebook Connect; Such services collect personally identifiable and non-personally identifiable information; Please make sure that You read and understand the 3rd Party Privacy Policies prior to using service.</p>
					<!-- <h5>7. Can you remove the information we retain?</h5><p>Yes! You can click _here_ and request to remove all the personal information we saved or click _here_ and remove the list of watched videos. Please note that this is irreversible.</p>  -->
					<h5>8. Emails and Promotional Activity:</h5>
					<p>We may contact you through email to sent updates and notifications, including promotional activities, and you may opt-out of these emails.</p>
					<h5>9. How to contact us?</h5>
					<p>In any case you feel that your privacy was violated, you may contact us directly by sending an email to privacy@stevie.com and detail the violation you found. Our privacy officer shall inspect your complaint and respond within a maximum of 14 days. Once per quarter, we will publish reports regarding the number of complaints we received and how we dealt with them.</p>
				</div> <!-- panel -->
			</div>	<!-- main -->	
		</div> <!-- page -->
	    <div id="footer">
			<?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/footer.php'); ?>
		</div>
	</body>
</html>
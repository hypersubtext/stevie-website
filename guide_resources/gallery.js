var objurl = '/server/services/guide_service.php?command=get_guide_channels';
var staticurl = 'https://264581f71178ddb995a3-2e9209cc5dc06342c8b27da3f0aef78b.ssl.cf2.rackcdn.com/';
var dotsHidden1 = true;
var dotsHidden2 = true;
var dotsHidden3 = true;
var myHost = document.domain;
var bigThumbsObj = (jQuery.parseJSON(fakeJson()).description);
setTimeout(function(){startSlideShows();},1000);


function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

function configSuccess(oresp) {
    if (oresp.responseText===undefined)
        resp = oresp;
    else
        resp = oresp.responseText;
    catObj = jQuery.parseJSON(resp).description;
    for (var key in catObj) {
        drawCat(catObj[key], key);
    }
}

function configFailed() {
    alert ('We had a problem getting the channel list. Try again later');
}

function arrowNum(c) {
    return (c%5)+1;
}

function drawCat (catObj, key) {
    var cHTML = '';
//     if (catObj.channels.length>5) {
         cHTML += '<div id="snake' + key + '" class="snake noLink">';
         cHTML += '<div class="leadCat noLink">';
         cHTML += '<a target="_blank" href="' + catObj.category.url + '"><img class="leadCatImg" src="' + catObj.category.style.guide_image + '">';
         cHTML += '<img class="leadCatPlayImg" src="' + staticurl + 'arrows/0.png">';
         cHTML += '<div class="leadCatPlayLabel">PLAY</div>';
         cHTML +='</a>';
         cHTML += '<div id="lead' + key + '" class="leadCatTitle">' + catObj.category.style.name + '</div>';
         cHTML += '</div>';
         cHTML += '<div id="Cat' + key + '" class="jMyCarousel jMyColors">';
         cHTML += '<ul style="list-style-type: none;">';
         for (var chKey in catObj.channels) {        
             cHTML += '<li class="snakeListItem"><a target="_blank" href="' + catObj.channels[chKey].url + '">';
             cHTML += '<img class="snakeImg" src="' + catObj.channels[chKey].style.guide_image + '">';
             cHTML += '<img class="snakePlayImg" src="' + staticurl + 'arrows/' + arrowNum(chKey) + '.png">';
             cHTML += '<div class="snakeChannelTitleBox noLink">';
             cHTML += '<span class="snakeChannelTitle noLink">';
             cHTML += catObj.channels[chKey].style.name;
             cHTML += '</span>';
             cHTML += '</div>';
             cHTML += '</a>';
             cHTML +='</li>';
         }
         cHTML += '</ul>';
         cHTML += '</div>';
         cHTML += '</div>';
         $("#allCats").append (cHTML);
         if ($("#lead" + key).height()>15) {
             $("#lead" + key).css("bottom",($("#lead" + key).height()+17) + "px");
         };

/*         $("#Cat" + key).scrollingCarousel({
             scrollSpeed: 'fast'
         });
*/
     
         $("#Cat" + key).jMyCarousel({
             visible: '700px', 
             speed: 300, 
             step: 50,
             easing: 'linear'
         }); 
     
//   }
 }

function startSlideShow1 () {
    jQuery('#slideshow1').jcarousel({
        auto: 5,
        wrap: 'circular',
        scroll: 1,
        easing: 'easeInOutSine',
        animation: "slow",
        buttonNextHTML: null,
        buttonPrevHTML: null,
        itemFallbackDimension: '228px',
        itemFirstInCallback: {onBeforeAnimation: setdot1}
    });
}

function startSlideShow2 () {
    jQuery('#slideshow2').jcarousel({
        auto: 5,
        wrap: 'circular',
        easing: 'easeInOutSine',
        scroll: 1,
        animation: "slow",
        buttonNextHTML: null,
        buttonPrevHTML: null,
        itemFallbackDimension: '228px',
        itemFirstInCallback: {onBeforeAnimation: setdot2}
    });
    setTimeout(function(){startSlideShow3();},1000);
}

function startSlideShow3 () {
    jQuery('#slideshow3').jcarousel({
        auto: 5,
        wrap: 'circular',
        easing: 'easeInOutSine',
        scroll: 1,
        animation: "slow",
        buttonNextHTML: null,
        buttonPrevHTML: null,
        itemFallbackDimension: '228px',
        itemFirstInCallback: {onBeforeAnimation: setdot3}
    });
    $("#bigChannels").css('visibility','visible');
    $("#allCats").css('visibility', 'visible');
}

function startSlideShows () {
/*    adjustToDevice(target); */
    populateBigThumbs (); 
    startSlideShow1();
    setTimeout(function(){startSlideShow2();},1000);

    jQuery.ajax({
       url: objurl,
       success: function(resp) {
           configSuccess(resp);
           },
       error: function(resp) {
           if ((resp.readyState===4)&&(resp.status===200)) {
                   configSuccess(resp);
           }
            else {
                configFailed();
           }
       },
       async:true,
       datatype: 'json'
     });

}

function populateBigThumbs () {
    var bigArrowID = 0;
    var divID = 1;
    var thisCat;
    var thisChan;
    var tHTML = '';

    for (var key in bigThumbsObj) {
        thisCat = bigThumbsObj[key];
        tHTML += '<div id="bigThumbDiv' + divID + '" class="bigThumbDiv">';
        tHTML += '<div class="bigThumbHeader">';
        tHTML += '<div class="bigThumbTitle">';
        tHTML += thisCat.name;
        tHTML += '</div>';
        tHTML += '<div style="z-index:999;" id="bigThumbsDots' + divID + '" class="bigThumbDots carousel-control">';
        for (var d=1;d<=3;d++) {
            tHTML += '<a href="#"><img onclick="dotclick(' + divID + ',' + d + ',true);"';
            if (d===1) 
                tHTML += 'src="http://cdn-guide.mystevie.com/dots/dot1.png"';
            else
                tHTML += 'src="http://cdn-guide.mystevie.com/dots/dot0.png"';
            tHTML += 'id="dot' + divID + '' + d + '" class="dot" /></a>';
        }
        tHTML += '</div>';
        tHTML += '</div>';
        tHTML += '<div class="bigCarousel">';
        tHTML += '<ul id="slideshow' + divID + '" class="jcarousel-skin-tango">';
        for (var i=0;i<3;i++) {
            thisChan = thisCat.channels[i];
            tHTML += '<li>';
            tHTML += '<a target="_blank" href="' + thisChan.url + '"><img class="bigThumbImg" src="' + thisChan.icon + '"/>';
            tHTML += '<img class="bigThumbPlayImg" src="http://cdn-guide.mystevie.com/arrows/big' + bigArrowID + '.png"></a>';
            tHTML += '<div class="bigThumbName noLink"><a target="_blank" href="' + thisChan.url + '">' + thisChan.title + '</a></div>';
            tHTML += '</li>';
            bigArrowID++;
            if (bigArrowID>5) {
                bigArrowID = 0;
            }
        }
        bigArrowID++;
        if (bigArrowID>5) {
            bigArrowID = 0;
        }
        tHTML += '</ul>';
        tHTML += '</div>';
        tHTML += '</div>';
        divID ++;
    }
    $("#bigChannels").append(tHTML);

}

function setdot1 (a,b,dotn,d) {
    if (dotsHidden1) {
        $("#bigThumbsDots1").show();
        dotsHidden1 = false;
    }
    dotclick(1, dotn, false);
}

function setdot2 (a,b,dotn,d) {
    if (dotsHidden2) {
        $("#bigThumbsDots2").show();
        dotsHidden2 = false;
    }
        dotclick(2, dotn, false);
}

function setdot3 (a,b,dotn,d) {
    if (dotsHidden3) {
        $("#bigThumbsDots3").show();
        dotsHidden3 = false;
    }
        dotclick(3, dotn, false);
}

function dotclick (cid, bigd, doMove) {
    var d = (bigd-1)%3+1;
    var carousel = jQuery('#slideshow' + cid).data('jcarousel');
    if (carousel!== undefined) {
        if ($('#dot' + cid + d).attr('src')=='http://cdn-guide.mystevie.com/dots/dot0.png') {
            switch (d) {
                case 1:
                    $('#dot' + cid + '1').attr('src','http://cdn-guide.mystevie.com/dots/dot1.png');
                    $('#dot' + cid + '2').attr('src','http://cdn-guide.mystevie.com/dots/dot0.png');
                    $('#dot' + cid + '3').attr('src','http://cdn-guide.mystevie.com/dots/dot0.png');
                    if (doMove) carousel.scroll(1);
                    break;
                case 2:
                    $('#dot' + cid + '1').attr('src','http://cdn-guide.mystevie.com/dots/dot0.png');
                    $('#dot' + cid + '2').attr('src','http://cdn-guide.mystevie.com/dots/dot1.png');
                    $('#dot' + cid + '3').attr('src','http://cdn-guide.mystevie.com/dots/dot0.png');
                    if (doMove) carousel.scroll(2);
                    break;
                case 3:
                    $('#dot' + cid + '1').attr('src','http://cdn-guide.mystevie.com/dots/dot0.png');
                    $('#dot' + cid + '2').attr('src','http://cdn-guide.mystevie.com/dots/dot0.png');
                    $('#dot' + cid + '3').attr('src','http://cdn-guide.mystevie.com/dots/dot1.png');
                    if (doMove) carousel.scroll(3);
                    break;
            }
        }
    }
}
function preload(arrayOfImages) {
    $(arrayOfImages).each(function(){
        $('<img/>')[0].src = this;
        // Alternatively you could use:
        // (new Image()).src = this;
    });
}

function preloadBigThumbs () {
    var imst = Array();
    for (var key in bigThumbsObj) {
        thisCat = bigThumbsObj[key];
        for (var i=0;i<3;i++) {
            thisChan = thisCat.channels[i];
            imst.push (thisChan.icon);
        }
    }
    preload(imst); 
}

function fakeJson () {
    var f = '{"status": 200, "description": [{"name": "Super Funny", "channels": [';
    f += '{"title": "Louis C.K", "url": "http:\/\/' + myHost + '\/louisck","icon": "http:\/\/graph.facebook.com\/louisck\/picture?width=280&height=175"},';
    f += '{"title": "The Lonely Island", "url": "http:\/\/' + myHost + '\/thelonelyisland", "icon": "http:\/\/graph.facebook.com\/thelonelyisland\/picture?width=280&height=175"},';
    f += '{"title": "Amy Schumer", "url": "http:\/\/' + myHost + '\/AmySchumer", "icon": "http:\/\/graph.facebook.com\/amyschumer\/picture?width=280&height=175"}';
    f += ']},';
    f += '{"name": "Super Cool", "channels": [{"title": "Designer for Tomorrow", "url": "http:\/\/' + myHost + '\/designerfortomorrow","icon": "http:\/\/graph.facebook.com\/designerfortomorrow\/picture?width=280&height=175"},';
    f += '{"title": "Extreme Sports Fans", "url": "http:\/\/' + myHost + '\/extremesportsfans","icon": "http:\/\/graph.facebook.com\/extremesportsfans\/picture?width=280&height=175"},';
    f += '{"title": "Art House Coop", "url": "http:\/\/' + myHost + '\/arthousecoop","icon": "http:\/\/graph.facebook.com\/arthousecoop\/picture?width=280&height=175"}';
    f += ']}';
    f += ',';
    f += '{"name": "Super Now", "channels": [{"title": "TED", "url": "http:\/\/' + myHost + '\/TED","icon": "http:\/\/graph.facebook.com\/TED\/picture?width=280&height=175"},';
    f += '{"title": "Fast Company", "url": "http:\/\/' + myHost + '\/fastcompany","icon": "http:\/\/graph.facebook.com\/fastcompany\/picture?width=280&height=175"},';
    f += '{"title": "Reuters", "url": "http:\/\/' + myHost + '\/reuters","icon": "http:\/\/graph.facebook.com\/reuters\/picture?width=280&height=175"}';
     f += ']}';
    f += ']}';

/*  f += '{"title": "Louis C.K", "url": "http:\/\/' + myHost + '\/LouisCKSzekely","icon": "http:\/\/graph.facebook.com\/LouisCKSzekely\/picture?width=280&height=175"},';
    f += '{"title": "The Lonely Island", "url": "http:\/\/' + myHost + '\/thelonelyisland","icon": "http:\/\/graph.facebook.com\/thelonelyisland\/picture?width=280&height=175"}';
    f += '{"title": "Amy Schumer", "url": "http:\/\/' + myHost + '\/AmySchumer", "icon": "http:\/\/graph.facebook.com\/amyschumer\/picture?width=280&height=175"}';
    f += ']},';
    f += '{"name": "Super Cool", "channels": [{"title": "Designer for Tomorrow", "url": "http:\/\/' + myHost + '\/designerfortomorrow","icon": "http:\/\/graph.facebook.com\/designerfortomorrow\/picture?width=280&height=175"},';
    f += '{"title": "Extreme Sports Fans", "url": "http:\/\/' + myHost + '\/extremesportsfans","icon": "http:\/\/graph.facebook.com\/extremesportsfans\/picture?width=280&height=175"},';
    f += '{"title": "Art House Coop", "url": "http:\/\/' + myHost + '\/arthousecoop","icon": "http:\/\/graph.facebook.com\/arthousecoop\/picture?width=280&height=175"}';
    f += ']}';
    f += ',';
   f += '{"name": "Super Now", "channels": [{"title": "TED", "url": "http:\/\/' + myHost + '\/TED","icon": "http:\/\/graph.facebook.com\/TED\/picture?width=280&height=175"},';
    f += '{"title": "Fast Company", "url": "http:\/\/' + myHost + '\/fastcompany","icon": "http:\/\/graph.facebook.com\/fastcompany\/picture?width=280&height=175"},';
    f += '{"title": "Reuters", "url": "http:\/\/' + myHost + '\/reuters","icon": "http:\/\/graph.facebook.com\/reuters\/picture?width=280&height=175"}';
     f += ']}';
    f += ']}';
*/
    return f;

}

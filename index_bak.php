<?php 
	$pageTitle = "home";
	$pageurl = "";
?><!DOCTYPE html>
<html>
	<head>
		<?php require('home/head.php'); ?>
		<link rel="stylesheet" href="home/pp/css/prettyPhoto.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="home/carousel/skins/tango/skin.css" type="text/css" media="screen" />
		<script type="text/javascript">
		function namespace(namespaceString) {
		    var parts = namespaceString.split('.'),
		        parent = document,
		        currentPart = '';    

		    for(var i = 0, length = parts.length; i < length; i++) {
		        currentPart = parts[i];
		        parent[currentPart] = parent[currentPart] || {};
		        parent = parent[currentPart];
		    }

		    return parent;
		}
		function setdot (a,b,dotn,d) {
			dotclick(dotn);
		}
		function dotclick (d) {
                    var carousel = jQuery('#slideshow').data('jcarousel');
                    if (carousel!=undefined) {
                        switch (d) {
                            case 1:
                                    $('#dot1').attr('src','http://static.mystevie.com/png/website/dot3.png');
                                    $('#dot2').attr('src','http://static.mystevie.com/png/website/dot0.png');
                                    $('#dot3').attr('src','http://static.mystevie.com/png/website/dot0.png');
                                    carousel.scroll(1);
                                    break;
                            case 2:
                                    $('#dot1').attr('src','http://static.mystevie.com/png/website/dot0.png');
                                    $('#dot2').attr('src','http://static.mystevie.com/png/website/dot2.png');
                                    $('#dot3').attr('src','http://static.mystevie.com/png/website/dot0.png');
                                    carousel.scroll(2);
                                    break;
                            case 3:
                                    $('#dot1').attr('src','http://static.mystevie.com/png/website/dot0.png');
                                    $('#dot2').attr('src','http://static.mystevie.com/png/website/dot0.png');
                                    $('#dot3').attr('src','http://static.mystevie.com/png/website/dot1.png');
                                    carousel.scroll(3);
                                    break;
                        }
                    }
		}
		function pushTwitter() {$.getScript('http://widgets.twimg.com/j/2/widget.js', function () {
                    var twitter = new TWTR.Widget({
                      version: 2, 
                      id: "twitter_div",
                      type: 'profile',
                      rpp: 100,
                      interval: 1000,
                      width: 200,
                      height: 40,
                      theme: {
                        shell: {
                          background: '#F1EEE7',
                          color: '#F1EEE7'
                        },
                        tweets: {
                          background: '#F1EEE7',
                          color: '#000000',
                          links: '#019AD2'
                        }
                      },
                      features: {
                        scrollbar: false,
                        loop: true,
                        live: true,
                        behavior: 'default'
                      }
                        }).render().setUser('MyStevieTV').start();;
                    })
		}
		</script>
		<script src="home/carousel/lib/jquery.jcarousel.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="home/pp/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
		
		
	</head>	
	<body onload="allLoaded();pushTwitter();drawVOD();drawLoggedIn();">

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=210504105683030";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>		


<div id="whiteZoneHomePage">
</div>

	<div id="canvas" class="homePage">
		<div id="topBar">
			<?php require('home/topbar.php'); ?>
		</div>
		
		<div id="main">		
            <div id="rightBar">
                
                <!-- contact for chromecast updates -->
                <script>
                    function sendMessage() {
                        var msgAddress = $("#emailBox").val();
                        $('#sendmail').submit();
                    }
                </script>

                <div id="chromecast_updates">
                    <span class="bolder">Stevie for Chromecast coming soon!</span>
                    <form name="sendmail" id="sendmail" method="post" action="send.php">
                        <div>
                            <label for="emailBox">Get notified:</label>
                            <input type="hidden" name="Topic" value="Chromecast">
                            <input id="emailBox" name="email" type="text" placeholder="Your Email Here" />
                            <a id="go_button" href="javascript: sendMessage();">Go</a>
                        </div>
                    </form>
                </div>

                <div id="likeFollow">
                    <!-- <div class="fb-like-box" data-href="https://www.facebook.com/MyStevieTV" data-width="200" data-show-faces="true" data-border-color='white' data-stream="false" data-header="false" style='background-color: white; margin-top: -5px'></div> -->
                    <div class="fb-like" data-href="https://www.facebook.com/MyStevieTV" data-send="false" data-width="180" data-show-faces="false"></div>
                    <a href="https://twitter.com/MyStevieTV" class="twitter-follow-button" data-show-count="true">Follow @MyStevieTV</a>
                </div>
                <div id="twitter_div"></div>
                <div style='margin-top: 25px; font-size: 17px; width: 189px; margin-left: -13px;font-family:arial;font-size:12px'>
                    Chosen to participate in:<br>
                    <a href='http://www.microsoftrnd.co.il/strategic-partnerships/microsoft-accelerator-for-windows-azure' target="_blank">
                        <img src='http://static.mystevie.com/png/website/MicrosoftAccelerator_white.png' style='padding-top: 3px;margin-left: 0px; width: 150px;'>
                    </a>
                </div>              
                <div style='padding-top: 20px; font-size: 17px; width: 189px; margin-left: -13px;'>
                    <a href='http://techcrunch.com/2012/05/21/stevie-social-tv-launch/' target="_blank"><img src='http://static.mystevie.com/png/website/disrupt_badge.png' style='padding-top: 0px'></a>
                </div>
                <div style='padding-top: 20px; font-size: 17px; width: 189px; margin-left: -13px;'>
                    <a href='http://blog.mipworld.com/2013/04/liveblog-mipcube-innovation-winners-2013-revealed/' target="_blank"><img src='http://static.mystevie.com/png/website/mipcube_badge.png' style='padding-top: 0px'></a>
                </div>
            </div> 
            <div id='middleBar'></div>
        <div id='leftImageLI' class="leftImage">
            <ul id='slideshow' class='jcarousel-skin-tango'>
            <li><div id='slide3' class='slide'>
            <div id='slide_heading3' class='slide_heading bolder'>Stevie<br/>for iPad<br/>& iPhone</div>
            <div class='slide_text' id='slide_text3'>Now with awesome<br/>AirPlay support!</div>
            <div class='slide_devices' id='slide_devices3'>
            <a href='http://itunes.apple.com/us/app/stevie/id547231007'>
            <img width='120' height='40' src='http://static.mystevie.com/png/website/ipad_big.png' />
            </a></div></div></li>

            <li>
            <div id='slide1' class='slide'><div id='slide_heading1' class='slide_heading bolder'>Stevie<br/>for Android</div>
            <div class='slide_text' id='slide_text2'>Designed for tablets,<br/>phones and Android TV</div><br/><br/>
            <div class='slide_devices' id='slide_devices2'>
            <a href='https://play.google.com/store/apps/details?id=com.stevie'>
            <img class='slide_store_icon' width='120' height='40' src='http://static.mystevie.com/png/website/play_big.png' />
            </a></div></div></li>

            <li><div id='slide2' class='slide' style='cursor: pointer;' onclick="document.location.href='guide';">
            <div id='slide_heading2' class='slide_heading bolder'>Stevie<br/>Guide</div>
            <div class='slide_text' id='slide_text2'>Watch hundreds<br/>of new channels.<br/>It's magic!</div>
            </div></li>
            </ul>
            <div id='dots' style='z-index:999;' class='carousel-control'>
            <a href='#'><img onclick='dotclick(1);' src='http://static.mystevie.com/png/website/dot3.png' id='dot1' class='dot' /></a>
            <a href='#'><img onclick='dotclick(2);' src='http://static.mystevie.com/png/website/dot0.png' id='dot2' class='dot' /></a>
            <a href='#'><img onclick='dotclick(3);' src='http://static.mystevie.com/png/website/dot0.png' id='dot3' class='dot' /></a>
        </div></div>
        <div id='leftImageLO' class="leftImage">
            <img src='http://static.mystevie.com/png/website/stevie_home_banner.png' style='margin-left: -10px;'/><br/>
            <a href='http://www.youtube.com/watch?v=PIM0N8KXKxQ' rel='prettyPhoto' title='What is Stevie?'>
            <img src='http://static.mystevie.com/png/website/whatisstevie.png' width='160' height='35' style='position: absolute; margin-top: -55px; margin-left: 5px;' /></a></div>
            <div id='ondemand' style='margin-top: 50px;'>
            <div id='ondemand_header' class='pretty_header'>Stevie On Demand</div>
            <div id='ondemand_header_text' class='pretty_header_text'>&nbsp;</div>
            <div id='ondemand_bar'></div>
        </div>
            <a id="link_belg" href="http://stievie.be" target="_blank">Op zoek naar Stievie? Klik hier.</a>
            <img id='ondemand_shadow' src='http://static.mystevie.com/png/website/shadow2.png'/>
            <div id='appsBar'>
                <div id='devices_header' class='pretty_header'>Get Stevie Apps</div>
                <div id='devices_header_text' class='pretty_header_text'>&nbsp;</div>
                <div id='appdiv'>
                        <a href="https://play.google.com/store/apps/details?id=com.stevie">
                            <img src='http://static.mystevie.com/png/website/googleplay_small.png' /> Stevie for Android
                        </a>
                        <a href='http://itunes.apple.com/us/app/stevie/id547231007'>
                                <img src='http://static.mystevie.com/png/website/appstore_small.png' /> Stevie for iPad
                        </a>
                        <a href='http://itunes.apple.com/us/app/stevie/id547231007'>
                                <img src='http://static.mystevie.com/png/website/appstore_small.png' /> Stevie for iPhone
                        </a>
                        <!--a href='http://apps.microsoft.com/webpdp/app/stevie/91c6b9c1-f436-4889-9f7e-e9f18e254089'>
                                <img src='http://static.mystevie.com/png/website/windows8store_small.png' /> Stevie for Windows 8
                        </a-->
                        <a href='http://itunes.apple.com/us/app/stevie-remote/id524963172'>
                                <img src='http://static.mystevie.com/png/website/appstore_small.png' /> Remote for iPhone
                        </a>                        
                </div>
            </div>
            <img id='apps_shadow' src='http://static.mystevie.com/png/website/shadow2.png'/>
            <br/>
		</div>
		<div id="fixedBottomBarItems">
                        <?php require('home/bottombar.php'); ?>
		</div>
	</div>
	<script type="text/javascript" charset="utf-8">
	$(document).ready(function(){
  		$("a[rel^='prettyPhoto']").prettyPhoto({
	    	theme: 'dark_rounded',
	    	social_tools: ''
		});
	  });
	</script>
    <?php
        require_once '/home/adroll.php';
    ?>
</body>
</html>

<?php
	$showPageTitle = $pageTitle;
	$sharerName = "";
	$sharerURL = "";
	$isOlympics = false;
	$isElection = false;
  	$httpChars = 'http://';
	if ($showPageTitle=="home") {
		$showPageTitle = "Creating super cool TV from your social media feeds";
	} 
    require('metaTags.php');
?>
<script type="text/javascript">
	var currentLogoID = 1;
    pageURL = '<?php echo $pageurl ?>';
    var pageTitle = '<?php echo $pageTitle ?>';
	var steviePath = '';
</script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="/home/webjs.js"></script>
<script type="text/javascript" src="/loader.js"></script>
<script type="text/javascript">
	// TODO: instead of this need to draw the buttons only after config success - daph
	if (config) {
    	var clientPath = configObj.client_path;
	} else if(twinHost.indexOf("www") !=-1 ) {
    	var clientPath = '<?php echo $httpChars; ?>' + twinHost + '/on/';
	} else  {
    	var clientPath = '<?php echo $httpChars; ?>' + twinHost + '/';
	}
</script>
<link rel="stylesheet" type="text/css" href="/home/website.css">
<title>Stevie TV - <?php echo $showPageTitle ?></title>
<script type="text/javascript">
	<?php 
		$isIE = strpos($_SERVER['HTTP_USER_AGENT'],'MSIE')>0;
		if ($isIE) {
			echo 'var isIE = true;' ;
		} else {
			echo 'var isIE = false;' ;
		}				
		echo "pageurl = '" . $pageurl . "';";											
		if (isset($_SERVER["HTTP_HOST"])) {
			echo "myHost = 'http://" . $_SERVER["HTTP_HOST"] . "';" ;
		}		
		if ($pageurl=='') {
			echo 'var isIndex = true;' ;
		} else {
			echo 'var isIndex = false;' ;
		}				
	?>
	WebFontConfig = {
		monotype: {
			projectId: '3b6f5a9f-d56c-4e16-9710-4dbe7d2a7c41'
		}
	};		      
	(function() {
		var wf = document.createElement('script');
		wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
		'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
		wf.type = 'text/javascript';
		wf.async = 'true';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(wf, s);
	})();
</script>	

<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-49968540-1']);
	_gaq.push(['_trackPageview']);

	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>

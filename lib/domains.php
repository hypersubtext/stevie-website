<?
	$links = array();

	$link = array(
		"website_domain" => "www.stevie.com",
		"app_domain" => "www.mystevie.com",
		"client_path" => 'www.mystevie.com/on'
	);

	array_push($links, $link);
	
	$link = array(
		"website_domain" => "dev.stevie.com",
		"app_domain" => "dev.mystevie.com", 
		"client_path" => 'dev.mystevie.com'
	);

	array_push($links, $link);

	$link = array(
		"website_domain" => "testim.stevie.com",
		"app_domain" => "dev.mystevie.com", 
		"client_path" => 'dev.mystevie.com'
	);

	array_push($links, $link);

	$link = array(
		"website_domain" => "daphna.stevie.com",
		"app_domain" => "daphna.mystevie.com", 
		"client_path" => 'daphna.mystevie.com'
	);

	array_push($links, $link);

	$link = array(
		"website_domain" => "daphna-air.stevie.com",
		"app_domain" => "daphna-air.mystevie.com", 
		"client_path" => 'daphna-air.mystevie.com'
	);

	array_push($links, $link);

	$link = array(
		"website_domain" => "gil-osx.stevie.com",
		"app_domain" => "gil-osx.mystevie.com", 
		"client_path" => 'gil-osx.mystevie.com'
	);

	array_push($links, $link);

	foreach ($links as $link) {
		if ($_SERVER['HTTP_HOST'] == $link['website_domain']) {
			define('CLIENT_PATH', 'http://'.$link['client_path']);
			break;
		}
	}
	
?>

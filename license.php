<?php 
	$pageTitle = "Stevie License Agreement";
	$pageurl = "license";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<?php require('home/head.php'); ?>
	</head>
	<body onload="allLoaded();">
		<div id="whiteZone">
		</div>

		<div id="canvas">
			<div id="topBar">
				<?php require('home/topbar.php'); ?>
			</div>
			<div id="main">		
				<div class="headline">
				</div>		
				<div class="simpleText" style='width: 700px'>
Stevie TV Ltd. hereby grants you a limited, worldwide, non-exclusive, non-assignable, non-sublicensable, non-transferable, revocable, temporary, personal license to use and run the Stevie Application on your personal computer, mobile device or tablet. You may not alter, amend, modify or otherwise interrupt with software nor may you transfer, distribute or otherwise publish software.
<br/><br/>
You acknowledge that Stevie is still in beta stages and therefore, Stevie provides you the software on an AS-IS basis and without any warranty or support. Stevie TV Ltd. shall never, and without any limit, be liable for any damage, cost, expense or any other payment incurred by yourself as a result of Software’s actions, failure, bugs and/or any other interaction between The Software  and your end-equipment, computers, other software or any 3rd party, end-equipment, computer or services. The Software is provided without any warranty; Stevie TV Ltd. hereby disclaims any warranty that The Software shall be error free, without defects or code which may cause damage to your computers or to yourself, and that Software shall be functional. You shall be solely liable to any damage, defect or loss incurred as a result of operating software and undertake the risks contained in running The Software on your end-equipment.
<br/><br/>
</div>
				<div id="bottomBarItems">
					<?php require('home/bottombar.php'); ?>
				</div>
				
			</div>
		</div>
    <?php
        require_once '/home/adroll.php';
    ?>
	</body>
</html>
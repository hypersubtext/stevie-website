<?php

    require_once($_SERVER['DOCUMENT_ROOT'] . "/lib/domains.php");

    $type = $pageType;
	$title = $pageTitle;
	$image = 'http://static.mystevie.com/png/website/logo/logo1.png';

	if ($pageType == 'landingPage') {
		$keywords = $channelKeywords;
		$description = $channelDescription;
		$image = $channelImage;
	} else {
		$keywords = 'Stevie,TV,Stevie TV,Connected TV,Social TV,Facebook,Twitter,Facebook TV,Twitter TV,Television';
		$description = 'Stevie is a channel that brings videos, status updates, tweets, events, birthdays, headlines & much more, and creates the greatest TV channel which is 100% yours!';
	}
?>

<title><?php echo $title ?></title>
<!-- Meta Tags -->
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<!-- <meta name="viewport" content="width=device-width, initial-scale=1" /> -->
<meta name="keywords" content="<?php echo $keywords; ?>" />
<meta name="description" property="og:description" content="<?php echo $description; ?>" /> 
<meta property="og:title" content="<?php echo $title; ?>" />
<meta property="og:type" content="website" /> 
<meta property="og:site_name" content="Stevie" />
<meta property="fb:app_id" content="210504105683030" /> 

<?php
	if(($pageType == 'landingPage') && ($embedUrlToMetaTags!="")) {
	    echo '<meta property="og:video" content="'.$embedUrlToMetaTags.'"/>';
	    echo '<meta property="og:video:type" content="application/x-shockwave-flash"/>';
	    echo '<meta property="og:video:width" content="960"/>';
	    echo '<meta property="og:video:height" content="720"/>';
	    echo '<meta property="og:image" content="' . $clipImage. '" />'; 
	}
	else {
	    echo '<meta property="og:image" content="' . $image . '" />'; 
	}
?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="/lib/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript">
	var clientPath = '<?php echo CLIENT_PATH; ?>';
	var pageurl = '<?php echo $pageurl; ?>';
</script>
<script type="text/javascript" src="/lib/script/website.js"></script>

<link type="text/css" rel="stylesheet" href="http://fast.fonts.net/cssapi/3b6f5a9f-d56c-4e16-9710-4dbe7d2a7c41.css"/>
<link href="/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="/lib/style/website.css" rel="stylesheet">

<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-49968540-1']);
	_gaq.push(['_trackPageview']);

	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>


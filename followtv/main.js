$(document).ready(function() {
	var myHost = document.domain;
	var twinHost = myHost.replace('stevie', 'mystevie');
	var clientPath;
        var firstChannel = getParameterByName("channelId");
        
        if(firstChannel!=null) {
            if(firstChannel.substring(0,3)=="%23") {
                $("#search_input").val("#" + firstChannel.substring(3));
                firstChannel = "%2523" + firstChannel.substring(3);
            }
            else {
                $("#search_input").val(firstChannel);
            }
        }

        if(twinHost.indexOf("www") !=-1 ) {
		clientPath = 'http://' + twinHost + '/on/';
	} else  {
		clientPath = 'http://' + twinHost + '/';
	}

    var host = clientPath;

	var servicesPath = clientPath+"server/services/";
	var clientArgs = "&component=website&platform=web";

	var errorCb = function(res) {
		console.error("Server error",res)
	}

	$(".topbar_fb").on('click', function() {
		window.location = clientPath + 'server/webclient/facebook_login.php?stevieurl=' + clientPath + 'FriendsTV'
	});

	$(".topbar_tw").click(function() {
		window.location = clientPath + 'server/webclient/twitter_login.php?stevieurl=' + clientPath + 'FriendsTV'
	})

	var whatPlaying;

	function doServerRequest(req,cb) {
		$.getJSON(servicesPath+req+clientArgs,function(res) {
			if(!res.status || !res.description) errorCb(res);
			if(res.status!=200) errorCb(res);
			cb(res.description);
		});
	}

	function replaceTrendingTopics(topics) {
		$("#topics").fadeOut("slow", function() { 
			$("#topics").html("");
			drawTrendingTopics(topics);
			$("#topics").fadeIn("slow");
		});
	}


    var colors = ['#F21C62', '#46C0FF', '#29B962', '#FFA200', '#9B74CD' ]

	function drawTrendingTopics(topics) {
		for(var i in topics) {
			var channel = topics[i];
			var myRel = '';
			if (!channel.extra_details) {
				myRel = 'Trending';
			} else {
				myRel = channel.extra_details.target;
			}


			var sum = 0;
			for(var i=0;i<channel.pretty_code.length;i++) {
    			sum += channel.pretty_code.charCodeAt(i);
			}
			var color = colors[sum%colors.length];

			var channelElem = $('<div class="topic channel" rel="'+myRel+'" style="color:'+color+'; border-left-color: '+color+'"><b>'+channel.pretty_code+'</b><span>&rsaquo;</span></div>');
			

			$(channelElem).data("channel",channel)
			channelElem.click(function() {
				//console.log('=>Trying to call channel ' + $(this).data("channel"));
				requestClientProgram($(this).data("channel"));
			});

			$(this).css('color', color);
			$(this).css('border-left-color', color);

			$("#topics").append(channelElem);
		}

		$(".topic").each(function() {
			// Random colors for channel squares
			// $(this).css( "color", function() {
			// 	colors = ['#F21C62', '#46C0FF', '#29B962', '#FFA200', '#9B74CD' ]
			// 	// colors #F5CF00 yellow, #29B962 green, #46C0FF blue, #FF5400 orange, #FF3F83 magenta(bad-magenta),'#F21C62 magenta' #E00 red, #9B74CD violet, 
			// 	return colors[Math.floor(Math.random()*colors.length)];
			// });

			// check if one of the topics is nowPlaying
			if ($(this).attr('rel')==whatPlaying) {
				$(this).addClass('selected');
			} else {
				$(this).removeClass('selected');
			}
		});
	}

    function requestClientProgram(channel) {
    	var pckg = {"cmd":"requestVODChannel","channel" : channel}
    	$("#stvplayer")[0].contentWindow.postMessage(pckg,"*");
        console.log("channel change",channel);
    }
    
    doServerRequest("trending_service.class.php?command=get",function(topics) {
    	drawTrendingTopics(topics);
        var pageTracker = _gat._getTrackerByName();
        if (firstChannel==null) {
            $("#stvplayer").attr("src", host+"/embed/trending?autoplay=yes&muted=no&color_scheme=dark");
            $("#topics .topic").first().addClass("selected");
        }
        else {
            $("#stvplayer").attr("src", host+"/embed/" + firstChannel + "?autoplay=yes&muted=no&color_scheme=dark");
        }
    });
    
    setInterval(function() {
        doServerRequest("trending_service.class.php?command=get",replaceTrendingTopics);
    },5000*60);

	var messageEventHandler = function(e){
         var packet = e.data;  
         console.log("client recieved packet",e.data)
         //$("#search_results").html("");
         if(packet.nowPlaying) {
         	whatPlaying = packet.nowPlaying;
         	//check if channel playing is in list
			$("#topics .topic").each(function() {
				if ($(this).attr('rel')!=whatPlaying) {
					$(this).removeClass('selected');
				} else {
					$(this).addClass('selected');
				}
			});
           	//$("#playing span").text(packet.nowPlaying)
         }
    }
    window.addEventListener('message', messageEventHandler,false);

    // Search Widget
	$.getScript(clientPath+'web_common/search.js', function() {
	    setTimeout(function() {
	    searchWidget = new Search(
	    	clientPath,
	    	$("#search_input"), // input dom (jQuery plz)
	    	$("#search_results"), // results dom
	    	"website", // website or client
	    	function(channel) {
	    		var pckg = {"cmd":"requestVODChannel","channel" : channel};
	    		$("#stvplayer")[0].contentWindow.postMessage(pckg,"*");
	    		$("#search_input").val(channel.style.name).blur().removeClass('highlight');
              	$("#search_results").html("");
              	$('iframe#stvplayer').focus();
	    	} // callback function for ordering a channel
	    	);
		},200)
	});

    function getEmbedHeight() {
		var embedHeight = ($("#stevie_embed").width() / 16 * 9);
		embedHeight += 40;
		$("#stevie_embed").css("height" , embedHeight + "px");
	}

	getEmbedHeight();
	$( window ).resize(function() {
		getEmbedHeight();
	});

	$("#search_input").on("focus",function(){
	    $(this).one('mouseup', function(event){
	        event.preventDefault();
	    }).select();
	    $("#search_results").show();
	});

	$("#search_input").on("blur", function(){
		if(!$("#search_results").is(":hover")) {
			$("#search_results").hide();			
		}
	})

        $("#footer_login_banner").click(function() {
            location.href = clientPath+"/server/webclient/twitter_login.php?stevieurl="+encodeURIComponent(clientPath+"/friendstv")
        });

});

function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}


<?php 
bottomMenuItem('About Stevie', 'about', true); 
bottomMenuItem('Contact Us', 'contact', false); 
bottomMenuItem('Privacy Policy', 'privacy', true); 

function bottomMenuItem($title, $url, $select) {
		global $pageurl;
		if (($select)&&($url==$pageurl)) {
			echo '<span class="selected">' . $title . '</span>';
		}
		else {
			echo '<a href="' . $url . '">' . $title . '</a>';
		}
		if ($url!='privacy') {
			echo ' | ';
		}
	}
?>
<?php 
    
    require_once($_SERVER['DOCUMENT_ROOT'] . "/lib/domains.php");

    $pageType = 'textPage';
    $pageTitle = 'Stevie Jobs';
    $pageurl = "jobs";
?>

<!DOCTYPE HTML>
<html>
	<head>
        <?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/head.php'); ?>
	</head>
	<body class="room">
		<div id="page" class="<?= $pageurl ?>">
			<?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/top_nav.php'); ?>
			<div id="main" class="container">
				<div class="panel">
					<h1 id="<?= $pageurl ?>" style="background-image : url(lib/style/<?= $pageurl ?>_title.png)"><?= $pageTitle ?></h1>
					<h2 id="about_title" style="padding-bottom: 10px;">Join the Stevies and help reimagine the future of TV and social media</h2>
                                        <img width="450" height="254" align="right" style="padding-left: 20px;" src="http://static.mystevie.com/website/Stevie_team01.jpg" />
					<p>
						Have you ever read about a startup exploding and thought 'god I wish I was there'? For once you could be in the right place at the right time when an entire industry changes! Stevie, a pioneering startup in a cutting edge industry - connected TV - is just waiting to disrupt the entire industry.<br/>
						Stevie is a platform that turns your social feeds into broadcast television, creating personal, monetizable entertainment across platforms.<BR/>
                                                The Stevie engine analyzes everything the viewers and their friends post, share, and like to determine relevance and taste. Along with feeds from brands, artists and celebrities, Stevie's patent pending line up algorithm creates 24/7 viewing experience form any topic or hashtag.<BR/>
					</p>
                                        <h3>Write us now - <a style="text-decoration: underline;" href="mailto:jobs@stevie.com">jobs@stevie.com</a></h3>
                                        
					<h4>Server side developer</h4>
                                        <img width="450" height="304" align="right" src="http://static.mystevie.com/website/Stevie_team02.jpg" />
					<p>
						* An experienced coder with great social skills<br/>
                                                * PHP, MySQL experience required<br/>
                                                * Multi platform whiz - Great advantage<br/> 
                                                * Big data understanding, great English and Media buff a big advantage
					</p>
					<h4>Client side developer</h4>
					<p>
						* An experienced coder with great social skills<br/>
                                                * Webapp development experience and deep understanding of webapp architecture<br/>
                                                * Strong JS, HTML5<BR/>
                                                * Proven track record with REST APIs (FB, Twitter, etc.)<br/>
                                                * Mobile, Java, Objective C - Great advantage<br/>                                                
                                                * Social media orientation, great English and Media buff a big advantage
					</p>
				</div> <!-- panel -->
			</div>	<!-- main -->	
		</div> <!-- page -->
	    <div id="footer">
			<?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/footer.php'); ?>
		</div>
	</body>
</html>

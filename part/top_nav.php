<div class="top_mask"></div>
<div id="top" class="navbar navbar-static-top" role="navigation">
    
    <div class="container">
        <a id="branding" class="navbar-brand" href="/">
            <img src="http://static.mystevie.com/png/website/logo_white.png" alt="Stevie. Your Friends On TV">
        </a>
        <div id="menu" class="navbar" role="navigation">
            <a id="press_link" href="/press">Press</a>
            <a id="blog_link" href="http://blog.stevie.com/">Blog</a>
            <a id="help_link" href="/help">Help</a>
            <a id="guide_link" href="/guide">Stevie Channel Guide</a>
            <a id="contact_link" href="/contact">Send Feedback</a>
        </div>

    <!-- 
        <div id="loginButtons" class="navbar-right">
            <img class="login_brdr" src="/home/images/login_brdr.png">
            <p>Login with</p>
            <div class="topbar_tw"><img src="/home/images/twitterIcon.png"></div>
            <div class="topbar_fb"><img src="/home/images/facebookIcon.png"></div>
        </div>
    -->
        
        <?php if ($pageType == 'landingPage') { ?>
            <div id="social_buttons" class="navbar-text navbar-right">
                <span>Like this channel?</span>
                <div class="social_buttons_btn" class="navbar-link">
                    <div class="fb-like" data-href="<?php echo $channel->landing_page_url ?>" data-layout="button" data-action="like" data-width="50" data-show-faces="false" data-share="false"></div>
                </div>
                <div class="social_buttons_btn" class="navbar-link">
                    <a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo $channel->landing_page_url ?>" data-via="MyStevieTV" data-colorscheme="dark" data-layout="button_count" data-related="StevieFavorites" data-count="none">Tweet</a>
                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                </div>
            </div>
        <? } else if ($pageType == 'textPage') { ?>
            <div id="social_buttons" class="navbar-text navbar-right">
                <div class="social_buttons_btn" class="navbar-link">
                    <div class="fb-like" data-href="https://www.facebook.com/MyStevieTV" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
                </div>
                <div class="social_buttons_btn" class="navbar-link">
                    <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://twitter.com/MyStevieTV" data-via="daphnap" data-count="none">Tweet</a>
                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                </div>
            </div>
        <? } else if ($pageType == 'homePage') { ?>
            <div class="navbar-right">
                <div id="homepage_social">
                    <a target="_blank" href="http://www.facebook.com/MyStevieTV"><img src="/home/images/facebookIcon.png"></a>
                    <a target="_blank" href="http://twitter.com/MyStevieTV"><img src="/home/images/twitterIcon.png"></a>
                </div>
                <a href="/about" id="awards_slider" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="lib/style/banners_mip.png" alt="Winner MIPCube Lab Competition">
                            <div class="carousel-caption">mip cube winner</div>
                        </div>
                        <div class="item">
                            <img src="lib/style/banners_tc.png" alt="Finalist TC Disrupt NY 2012">
                            <div class="carousel-caption">TC Disrupt finalist</div>
                        </div>
                        <div class="item">
                            <img src="lib/style/banners_azure.png" alt="Participant Microsoft Accelerator">
                            <div class="carousel-caption">Windows Azure accelerator</div>
                        </div>
                    </div>
                </a>
            </div>
        <? } ?>

    </div>
</div>
<?php
	$pageurl = "sendInvitation";
	require_once('home/mailer.php');
	
	function isvalid($address) {
		return ($address!="") ; 
	}

    $senderName = $_REQUEST["name"];
    $senderEmail = $_REQUEST["senderemail"];
    $address1 = trim($_REQUEST["email1"]);
	$address2 = trim($_REQUEST["email2"]);
	$address3 = trim($_REQUEST["email3"]);
	$address4 = trim($_REQUEST["email4"]);

	$subject = $senderName . " invited you to Stevie";
	
	$body = trim(strip_tags($_REQUEST["body"]));
	$message = $body;
	$rand = rand(100000,999999);
	$message = $message . "\r\n\r\nGo get your Stevie: http://stevie.com/invited.php?id=" . $rand;
	$message = wordwrap($message, 70);
	
	$emailSent1 = true;
	$emailSent2 = true;
	$emailSent3 = true;
	$emailSent4 = true;
	
	$replyto = $senderEmail;
	$stevieSender = "invitation@stevie.com";

    if (isvalid($address1)) {
      $emailSent1 = sendStevieMail($stevieSender, $address1, $subject, $message, $replyto);
	}

	if (isvalid($address2)) {
      $emailSent2 = sendStevieMail($stevieSender, $address2, $subject, $message, $replyto);
	}

	if (isvalid($address3)) {
      $emailSent3 = sendStevieMail($stevieSender, $address3, $subject, $message, $replyto);
	}

	if (isvalid($address4)) {
      $emailSent4 = sendStevieMail($stevieSender, $address4, $subject, $message, $replyto);
	}

	if (($address1 . $address2 . $address3 . $address4)=="") {
		$err = "Please fill in at least one email address.<br/><br/><a class='greenStevie' href='invite.php'>Try again</a>";
	}
	else
	{
		if ($emailSent1&&$emailSent2&&$emailSent3&&$emailSent4) {
			$err = "Sent successfully.<br/><br/>Thank you for inviting your friends!<br/><br/><a class='greenStevie' href='invite.php'>Invite more</a>";
		}
		else {
			$problems = "";
			if (!$emailSent1) {
			   $problems .= $address1 . ', ';
		    }
			if (!$emailSent2) {
			   $problems .= $address2 . ', ';
		    }
		    if (!$emailSent3) {
			   $problems .= $address3 . ', ';
		    }
		    if (!$emailSent4) {
			   $problems .= $address4 . ', ';
		    }
	    	$err = 'We had a problem sending invitations to ' . $problems . ' sorry..<br/>Please try again later or email us at <a class="greenStevie" href="mailto:support@stevie.com">support@mystevie.com</a>. Thanks';
		}
		if ($emailSent1&&$emailSent2&&$emailSent3&&$emailSent4) {
			$pageTitle = "Thank you!";
		}
		else {
			$pageTitle = "Oops...";
		}
	}


?>



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<?php require('home/head.php'); ?>
	</head>
	<body onload="allLoaded();$('#watchStevie').css('left','745px');">
		<div id="whiteZone">
		</div>

		<div id="canvas">
			<div id="topBar">
				<?php require('home/topbar.php'); ?>
			</div>
			<div id="fixedBottomBarItems">
				<?php require('home/bottombar.php'); ?>
			</div>
			<div id="main">
				<div class="headline">
				<?php
					echo $err; 
				?>
				</div>		
			</div>
		</div>
	</body>
</html>

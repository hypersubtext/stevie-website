<?php
	echo '<meta name="keywords" content="Stevie,TV,Stevie TV,Connected TV,Social TV, Facebook, Twitter, Faacebook TV, Twitter TV, Television">';
	echo '<meta name="viewport" content="width=1280">';
	if ($pageurl=="watch") {
		echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
		echo '<meta property="og:title" content="' . $videoTitle . '"/>'; 
		echo '<meta property="og:image" content="' . $videoImage . '"/>'; 
		echo '<meta name="description" property="og:description" content="' . $videoDescription . '"/>'; 
		echo '<meta property="og:site_name" content="Stevie Share"/>'; 
		echo '<meta property="fb:app_id" content="210504105683030"/>'; 
	}
	else {
		echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
		echo '<meta property="og:title" content="' . $showPageTitle . '"/>'; 
		echo '<meta property="og:type" content="website"/>';
		echo '<meta property="og:image" content="http://cdn-p.mystevie.com/Retsef/client_flavors/ipad/full_logo@2x.png"/>'; 
		echo '<meta property="og:site_name" content="Stevie"/>';
		echo '<meta property="fb:app_id" content="210504105683030"/>'; 		
                echo '<meta property="og:description" name="description" content="Stevie is a channel that brings videos, status updates, tweets, events, birthdays, headlines & much more, and creates the greatest TV channel which is 100% yours!" />';
	}
?>
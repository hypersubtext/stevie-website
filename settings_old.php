<?php 
$pageTitle = "Stevie Settings (old)";
$pageurl = "settings";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<?php require('home/head.php'); ?>
<script>
			var logOutURL;
			var twitterLoginURL;
			var twitterLogoutURL;
			var twitterConnectURL;
			var twitterDisconnectURL;
			var isConnections;
			var isTwitter;
			var mailistData;

			$(function(){  
				  $("html").bind("ajaxStart", function(){  
				     $(this).addClass('busy');  
				   }).bind("ajaxStop", function(){  
				     $(this).removeClass('busy');  
				   });  
				});

			function getListName(listID, defaultName) {
				switch (listID) {
					case '58a3af5030':
						return 'New features, new channels, and stuff related directly to you';
						break;
					case 'aac5fa51fa':
						return 'The Stevie newsletter with tips and special offers';
						break;
					default:
						return defaultName;
				}
			}
			
			function gourl (url) {
				document.location.href = url;
			}
			
			function twitterLogout() {
				gourl (twitterLogoutURL);
			}

			function twitterLogin() {
				gourl (twitterLoginURL);
			}

			function toggleTwitter(checkObject, doConnect) {
				var myurl;
				var diditwork = false;
				if (doConnect) {
					myurl = twitterConnectURL;
				}
				else {
					myurl = twitterDisconnectURL;
				} 
				jQuery.ajax({
					   url: myurl,
					   success: function(resp) {
					     diditwork = (jQuery.parseJSON(resp).description.result=='success'); 
					   },
					   async:false,
					   datatype: 'json',
					   xhrFields: {
						      withCredentials: true
						   }
					 });
				if (!diditwork) {
					checkObject.checked = !doConnect;
					alert ('There was a problem with Twitter, please try again later');
				}
			}

			function toggleMailing(checkObject, subscribeURL, unsubscribeURL) {
				var checked = checkObject.checked;
				var diditwork = false;

				if (checked) {
					jQuery.ajax({
						   url: subscribeURL  + '&format=json',
						   success: function(resp) {
						     diditwork = (jQuery.parseJSON(resp).description.result=='success'); 
						   },
						   async:false,
						   datatype: 'json',
						   xhrFields: {
							      withCredentials: true
							   }
						 });
					 if (!diditwork) {
						 checkObject.checked = false;
						 alert ('There was a problem subscribing you, please try again later');
					 }
				}
				else {
					jQuery.ajax({
						   url: unsubscribeURL  + '&format=json',
						   success: function(resp) {
							     diditwork = (jQuery.parseJSON(resp).description.result=='success'); 
						   },
						   async:false,
						   datatype: 'json',
						   xhrFields: {
							      withCredentials: true
							   }
						 });
					 if (!diditwork) {
						 checkObject.checked = true;
						 alert ('There was a problem unsubscribing you, please try again later - or write support@stevie.com');
					 }
				}
			}
			
			function drawSettings(data) {
				var oneList;
				logOutURL = data.urls.logout;
				logOutURL = data.urls.logout;
				twitterLoginURL =  data.urls.twitter_authorize;
				twitterLogoutURL = data.urls.twitter_logout;
				twitterConnectURL =   data.urls.twitter_connect  + '&format=json';
				twitterDisconnectURL =  data.urls.twitter_disconnect  + '&format=json';
				mailistData = data.mailinglists;
 				isMailist = (mailistData!=undefined);
				isConnections = (data.connections!=undefined);
				if (isConnections) {
					isTwitter = (data.connections.twitter!=undefined);
				}
				else {
					isTwitter = false;
				}
				var connectionsHTML = '';
				if (isTwitter) {
					if (data.connections.twitter.connected == "1") {
						var twitterChecked = '<div id="toggleTwitterDiv" class="hdiv25px tall">';
						twitterChecked = twitterChecked  + '<input id="toggleTwitter" onclick="toggleTwitter(this,false)" type="checkbox" checked />';
					}
					else {
						var twitterChecked = '<div id="toggleTwitterDiv" class="hdiv25px tall">';
						twitterChecked = twitterChecked  + '<input id="toggleTwitter" onclick="toggleTwitter(this,true)" type="checkbox" />';
					}
					twitterChecked = twitterChecked + ' Show content from twitter</div>';
					connectionsHTML = connectionsHTML + 
					'<img class="settingsMyPic" src="' + data.connections.twitter.picture + '" />' +
					'<img class="settingsFacebookPic" src="http://static.mystevie.com/png/twitter.png" />' +
					'<span class="settingsMyName">' + data.connections.twitter.username + '</span>' +
					'<span class="settingsLogoutLink"><a href="javascript:twitterLogout()">' +
					'Log out</a></span>' +
					twitterChecked;
				} 
				else {
					connectionsHTML = connectionsHTML + '<div id="settingsTwitterLogin" style="margin-left: 50px;">' +
									'<img class="settingsFacebookPic" id="twitterLoginPic" src="http://static.mystevie.com/png/twitter.png" /> <a href="javascript:twitterLogin()" style="position: relative; top:-10px;">Connect your Twitter</a></div>';
				}

				if (isMailist) {
					var mailingChecked = '';
					for (var listn=0;listn<mailistData.length;listn++) {
						oneList = mailistData[listn];
						mailingChecked += '<div class="toggleMailingDiv">';
						if (oneList.subscribed) {
							mailingChecked += '<input class="toggleMailing" onclick="toggleMailing(this,\'' + oneList.urls.subscribe + '\',\'' + oneList.urls.unsubscribe + '\')" type="checkbox" checked />';
						}
						else {
							mailingChecked += '<input class="toggleMailing" onclick="toggleMailing(this,\'' + oneList.urls.subscribe + '\',\'' + oneList.urls.unsubscribe + '\')" type="checkbox" />';
						}
						mailingChecked += ' ' + getListName(oneList.id, oneList.name) +'</div>';
					}
				}
				var mHTML = 
					'<div class="bolder tall" style="width: 400px; margin-bottom: 5px;">Stevie will only send you email about:</div>' +
					mailingChecked +
					'</div>';

				var fHTML = 
					'<div class="bolder tall">Connected accounts:</div>' +
					'<div class="settingsLoggedInAs">' +
					'<img class="settingsMyPic" src="' + data.user.picture + '" />' +
					'<img class="settingsFacebookPic" src="http://static.mystevie.com/png/Facebook.png" />' +
						'<span class="settingsMyName">' + data.user.name + '</span>' +
					'</div>';
				var sHTML = 
					'<div class="settingsLoggedInAs">' +
					connectionsHTML +
					'</div>';
				$("#settingsForm").append(fHTML);
				$("#settingsForm").append(sHTML);
				$("#settingsForm").append('<div id="EmailNotifications">' + mHTML + '</div>');
			}
			function outputSettings(JSONdata) {
				if (!loggedIn) {
					document.location.href = clientPath + 'server/webclient/login.php?stevieurl=' + myHost + '/settings';
				}
                                else {
                                    var logOutURL = "";
                                    var twitterLoginURL = "";
                                    var twitterLogoutURL = "";
                                    var twitterConnectURL = "";
                                    var twitterDisconnectURL = "";
                                    drawSettings(JSONdata);
                                }
			}
		</script>
</head>
<body onload="allLoaded();">
	<div id="whiteZone"></div>

	<div id="canvas">
		<div id="topBar">
			<?php require('home/topbar.php'); ?>
		</div>
		<div id="fixedBottomBarItems">
			<?php require('home/bottombar.php'); ?>
		</div>
		<div id="main">
			<div id="settingsForm" class="headline FAQlinks"></div>
		</div>
	</div>
</body>
</html>

<?php 
	$pageurl = "unsubscribed";
	$pageTitle = "Mailing subscription";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<?php require('home/head.php'); ?>
	</head>
	<body onload="allLoaded();">
		<div id="whiteZone">
		</div>

		<div id="canvas">
			<div id="topBar">
				<?php require('home/topbar.php'); ?>
			</div>
			<div id="fixedBottomBarItems">
				<?php require('home/bottombar.php'); ?>
			</div>
			<div id="main">
				<div class="headline">
                                    You've been successfully unsubscribed.<br/>
                                </div>
                            <div class='pressText pressLink'>
                                    &nbsp;&nbsp;&nbsp;&nbsp;<a href='settings' >Mailing preferences</a>
                            </div>
                        </div>		
		</div>
    <?php
        require_once '/home/adroll.php';
    ?>
	</body>
</html>
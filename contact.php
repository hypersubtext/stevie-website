<?php 
    
    require_once($_SERVER['DOCUMENT_ROOT'] . "/lib/domains.php");

    $pageType = 'textPage';
    $pageTitle = 'Contact Us';
    $pageurl = "contact";
?>

<!DOCTYPE HTML>
<html>
	<head>
        <?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/head.php'); ?>
        <script>
			function sendMessage() {
				var msgAddress = $("#emailBox").val();
				var msgSubject = $("#subjectBox").val();
				var msgBody = $("#bodyBox").val();
				$('#sendmail').submit();
			}
		</script>
	</head>
	<body class="room">
		<div id="page" class="<?= $pageurl ?>">
			<?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/top_nav.php'); ?>
			<div id="main" class="container">
				<div class="panel">
					<h1 id="<?= $pageurl ?>" style="background-image : url(lib/style/<?= $pageurl ?>_title.png)"><?= $pageTitle ?></h1>
					<div class="row">
						<div class="col-md-6">
							<h4>You can reach us here:</h4>
							<p>Support - <a href="mailto:support@stevie.com">support@stevie.com</a></p>
							<p>Webmaster - <a href="mailto:webmaster@stevie.com">webmaster@stevie.com</a></p>
							<p>Love us? Drop us a line - <a href="mailto:love@stevie.com">love@stevie.com</a></p>
							<p>Or give us your feedback - <a href="mailto:webmaster@stevie.com">webmaster@stevie.com</a></p>
						</div>
						<div id="contactBox" class="col-md-6">
							<form class="form-horizontal" role="form" name="sendmail" id="sendmail" method="post" action="send.php">
								<div class="row">
									<h4>Contact Us</h4>
									<div class="form-group">
										<label for="emailBox" class="col-sm-3 control-label">Your Email:</label>
										<div class="col-sm-9">
											<input id="emailBox" class="form-control" placeholder="Email" name="email" type="text" />
										</div>
									</div>
									<div class="form-group">
										<label for="subjectBox" class="col-sm-3 control-label">Subject:</label>
										<div class="col-sm-9">
											<input id="subjectBox" class="form-control" placeholder="Subject" name="subject" type="text" />
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-9 col-sm-offset-3">
											<textarea id="bodyBox" class="form-control" rows="8" name="body"></textarea>
										</div>
									</div>
									<div id="sendButton" class="btn btn-default btn-lg col-sm-3 col-sm-offset-6"><a href="javascript: sendMessage();">Send</a></div>
								</div>
							</form>
						</div>
					</div>
				</div> <!-- panel -->
			</div>	<!-- main -->	
		</div> <!-- page -->
	    <div id="footer">
			<?php require_once($_SERVER['DOCUMENT_ROOT'] .'/part/footer.php'); ?>
		</div>
	</body>
</html>

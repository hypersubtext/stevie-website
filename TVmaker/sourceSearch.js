// requires jQuery

// constructor: jQuery-object domElement, {website,client} component, callbackToOrdersource, boolean autoComplete
// domElement must have at least 2 children. .input which is a text field and .results for the results.

// only one public method, search.go(callback);
// that searches with the current text field value.
// Callback will be fired after results dom has been populated with results.
var batchid = 0;
function Search(host,domElementInput,domElementResults,component,ordersourceCallback) {
    var clientArgs = "&component="+component+"&platform=web";

    var errorCb = function(res) {
        console.error("Server error",res)
    }

    var typeTimer = null;
    var pendingRequestHandler = null;
    // when user is waiting for timer and/or server request

    var userPressedKey = 0; // 0 - user hasn't pressed a key yet

    var resultsDom = domElementResults;
    var inputDom = domElementInput;

    var sourceToCall;
    var that = this;

    function doServerRequest(req,cb) {
      if (pendingRequestHandler != null) {
        pendingRequestHandler.abort();
      }
      pendingRequestHandler = $.getJSON(req+clientArgs,function(res) {
          pendingRequestHandler = null;
          if(!res.status || !res.description) errorCb(res);
          if(res.status!=200) errorCb(res);
          cb(res.description);
      });
    }
        
    function query(query) {
      ++batchid;
      $("#ajaxGIF").show();
      doServerRequest("search_service.php?command=search&q="+query+"&limit=5&batch=" + batchid,function(res) {
        if(inputDom.val()=="") return;
        resultsDom.html("");
        suggestedSources = res.suggestedSources;
        $("#ajaxGIF").hide();
        for(var i in suggestedSources) {
          var source = suggestedSources[i];
          var sourceElem = $('<li class="source">'+source.name+'<span>Add</span></li>');
          $(sourceElem).data("source",source)
          sourceElem.click(function() {
              var source = $(this).data("source");
              var thisURL = source.url;
              var mainString = "";
              if (thisURL.substr(8,8)=='facebook') {
                  mainString = "Make main FB source";
              }
              var sourceBox = '<a class="sourceURL" id="sourceURLitem' + source.id + '" target="_blank" href="' + source.url + '">' + source.name + '</a><a href="javascript:delSource(\'' + source.id +'\');"><span class="sourceDelSpan">x</span></a><a href="javascript:makeMainSource(\'' + source.id +'\');"><span id="mainLink' + source.id + '" class="sourceMainSpan">' + mainString + '</span></a>';
              $("#sourceList").append('<div class="sourceItemClass" id="SIC' + source.id + '">' + sourceBox + '<br/><br/></div>');
              $(this).remove();
          })
          resultsDom.append(sourceElem);
        }


        // in case user presses enter or arrow keys before last query finished OR autocomplete timer still running
        if (userPressedKey != 0) {
          keyPress(userPressedKey);
          userPressedKey = 0;
        }
      });        
    }
    
    function keyPress(keyCode) {
      var isSelected = ($(".selected_result").length>0);
      var isResults = (resultsDom.children().length>0);
      var scrollToElement = $(".selected_result");

      switch (keyCode) {
        case 27: //esc - clears search and results
          inputDom.val("");
          resultsDom.html("");
        break;
        case 13: //enter
          if(isSelected) { // check if there is a selected_result
            $(".selected_result").trigger("click");
          } else {
            ordersourceCallback(sourceToCall);
          }
        break;
        case 40: //down
          event.preventDefault();
          if(isResults) {
            var nextItem;
            if(!isSelected) { // input
              nextItem = resultsDom.children().first();
              inputDom.removeClass('highlight');
              resultsDom.children().first().addClass("selected_result");
            } else if($(".selected_result").next().length > 0) {
              nextItem = $(".selected_result").next();
              $(".selected_result").removeClass("selected_result").next().addClass("selected_result");
            } else { // reached end of list
              nextItem = $(".selected_result");
              $(".selected_result").removeClass("selected_result");
              inputDom.addClass('highlight');
            }
            // scroll selected item into view
            if((nextItem.position().top + nextItem.height()) > resultsDom.height() || (nextItem.position().top - nextItem.height()) < 0) {
              resultsDom.animate({ scrollTop: nextItem.position().top + resultsDom.scrollTop() });
            }
          }
        break;
        case 38: // up
          event.preventDefault();
          if(isResults) {
            var prevItem;
            if(isSelected) {
              if($(".selected_result").prev().length > 0) {
                prevItem = $(".selected_result").prev();
                $(".selected_result").removeClass("selected_result").prev().addClass("selected_result");
              } else { // at beginning of list
                prevItem = $(".selected_result");
                $(".selected_result").removeClass("selected_result");
                inputDom.addClass('highlight');
              }
            }

            // scroll selected item into view
            if( isSelected && prevItem.position().top < 0 ) {
              resultsDom.scrollTop(prevItem.position().top + resultsDom.scrollTop());
            }
          }
        break;
      }
    }

    inputDom.on("keydown",function(event) {

      if((event.keyCode!=13) && (event.keyCode!=38) && (event.keyCode!=40)) {
          clearTimeout(typeTimer);
          typeTimer = setTimeout(function() {
            typeTimer = null;
            that.go();
          },100);
      } else {
        if((pendingRequestHandler != null) || (typeTimer != null)) {
          // register key press if timer or query hasn't finished
          userPressedKey = event.keyCode;
        } else {
          keyPress(event.keyCode);
        }
      }
    });

    this.go = function() {
      query(encodeURIComponent(inputDom.val()));
    };

    this.requestsourceFromInputVal = function() { // to order source by clicking go button in webiste
      ordersourceCallback(sourceToCall);
    }

}


<?php
	set_include_path(get_include_path() . PATH_SEPARATOR . $path);
	require_once('include.inc.php');
	require_once('model/user.class.php');
	require_once('dao/user_dao.class.php');
	
	require_once('logic/user_logic.class.php');
	require_once('connectors/facebook_connector.class.php');	
	require_once('logic/invitation_logic.class.php');
	
	
	$user = user_logic::get_logged_in_user();
	$isLoggedIn = ($user != null);

	
	invitation_logic::detect_invitation();
	

	$validInvitation = invitation_logic::has_valid_invitation();

	
	
	//	debug_print($user);
	
	
	
//	debug_print($user);
   	//id, name, first_name, last_name, facebook_link, username
    //birthday, email, timezone, locale, facebook_updated_time
    //facebook_id, stevie_secret, facebook_access_token
?>